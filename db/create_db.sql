USE `cinema`;

CREATE TABLE `system_role` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` nvarchar(20) UNIQUE NOT NULL
);

CREATE TABLE `user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `login` nvarchar(50) UNIQUE NOT NULL,
  `password` nvarchar(20) NOT NULL,
  `nickname` nvarchar(30) NOT NULL,
  `system_role_id` int NOT NULL
);

CREATE TABLE `film` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `original_name` nvarchar(150) NOT NULL,
  `imdb_rating` float,
  `image_dir` varchar(150),
  `timekeeping` time NOT NULL
);

CREATE TABLE `offer` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `film_id` int NOT NULL,
  `cinema_hall_id` int NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `price` int NOT NULL
);

CREATE TABLE `cinema_hall` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `size` int NOT NULL,
  `image_dir` varchar(150)
);

CREATE TABLE `cinema_hall_translate` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `language_id` int NOT NULL,
  `cinema_hall_id` int NOT NULL,
  `name` nvarchar(30) NOT NULL
);

CREATE TABLE `order` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `ticket_id` int NOT NULL,
  `user_id` int NOT NULL,
  `place` int NOT NULL,
  `is_paid` boolean
);

CREATE TABLE `language` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(2) NOT NULL
);

CREATE TABLE `film_translate` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `language_id` int NOT NULL,
  `film_id` int NOT NULL,
  `name` nvarchar(150) NOT NULL,
  `description` nvarchar(1000) NOT NULL
);

ALTER TABLE `user` ADD FOREIGN KEY (`system_role_id`) REFERENCES `system_role` (`id`);

ALTER TABLE `offer` ADD FOREIGN KEY (`film_id`) REFERENCES `film` (`id`);

ALTER TABLE `offer` ADD FOREIGN KEY (`cinema_hall_id`) REFERENCES `cinema_hall` (`id`);

ALTER TABLE `cinema_hall_translate` ADD FOREIGN KEY (`cinema_hall_id`) REFERENCES `cinema_hall` (`id`);

ALTER TABLE `cinema_hall_translate` ADD FOREIGN KEY (`language_id`) REFERENCES `language` (`id`);

ALTER TABLE `order` ADD FOREIGN KEY (`ticket_id`) REFERENCES `offer` (`id`);

ALTER TABLE `order` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

ALTER TABLE `film_translate` ADD FOREIGN KEY (`film_id`) REFERENCES `film` (`id`);

ALTER TABLE `film_translate` ADD FOREIGN KEY (`language_id`) REFERENCES `language` (`id`);
