USE `cinema`;

INSERT INTO `cinema`.`language`(name)
VALUES('en'), ('ru');

INSERT INTO `cinema`.`film`(original_name, imdb_rating, timekeeping)
VALUES('The Shawshank Redemption', 9.3, '2:25:00'),
('The Green Mile', 8.6, '2:07:00'),
('The Godfather', 9.2, '3:29:00'),
('Léon', 8.5, '1:58:00'),
('Gisaengchung / Parasite', 8.6, '2:31:00');

INSERT INTO `cinema`.`film_translate`(film_id, language_id, name, description)
VALUES
( 1, 1,'The Shawshank Redemption'
,'The cult drama is based on the novel by Stephen King "Rita Hayworth of the Shawshank". The film is set in the late 50s of the last century. Young financier Andy Dufrein is sentenced to life imprisonment for double murder, but he categorically denies any involvement in the crime. Andy is sent to serve his term in one of the most famous prisons - Shawshank, from which no one has yet managed to escape. In prison, the guy is faced with the harsh realities of the prison world, a world where there is no place for compassion, but around only cruelty. Andy repeatedly becomes a victim of sexual harassment, but he resists to the last. Soon, Dufrein''s prison life improves a little, and he even finds a friend for himself, who becomes a black prisoner Red ...'),
(1, 2, 'Побег из Шоушенка', 
'В основу культовой драмы легла повесть Стивена Кинга «Рита Хейуорт из Шоушенка». Действие фильма разворачивается в конце 50-х годов прошлого столетия. Молодого финансиста Энди Дюфрейна за двойное убийство приговаривают к пожизненному заключению, но он категорически отрицает свою причастность к преступлению. Свой срок Энди отправляется отбывать в одну из известнейших тюрем – Шоушенк, из которой еще никому не удавалось сбежать. В тюрьме парень сталкивается с суровыми реалиями тюремного мира, мира, где нет места состраданию, а вокруг только жестокость. Энди неоднократно становится жертвой сексуальных домогательств, но он сопротивляется до последнего. В скором времени тюремная жизнь Дюфрейна немного налаживается, и он даже находит себе друга, которым становится чернокожий заключенный Рэд...'),
(2, 1, 'The Green Mile', 
'The film is based on the novel of the same name by Stephen King. One day, a new prisoner enters the block for those sentenced to death - John Coffey, convicted of killing little girls. Coffey attracts the attention of the head of this unit - Paul Edgecomb. Over the years, he has seen enough of various criminals, and he knows a little about people. Paul doubts Coffey''s guilt, because despite his terrifying appearance, he has kindness and humanity. It also turns out over time that John is the owner of an amazing gift - the ability to heal. Paul understands that such a person cannot possibly be a murderer. But what to do and how to help him?'),
(2, 2, 'Зеленая миля', 
'Картина снята по одноименному роману Стивена Кинга. Однажды в блок для приговоренных к смертной казни поступает новый заключенный – Джон Коффи, осужденный за убийство маленьких девочек. Коффи привлекает внимание начальника этого блока – Пола Эджкомба. За долгие годы работы он насмотрелся на разных преступников, и он немного разбирается в людях. Пол сомневается в виновности Коффи, ведь несмотря на его устрашающий вид, он обладает добротой и человечностью. Так же со временем выясняется, что Джон является обладателем удивительного дара – способностью исцелять. Пол понимает, что такой человек никак не может быть убийцей. Но что же делать, и как помочь ему?'),
(3, 1, 'The Godfather', 
'The iconic picture is based on the novel of the same name by the American writer Mario Puzo. It''s 1945. In the center of the plot is the Sicilian mafia family Corleone, which has a tough criminal position in New York. The head of the family Vito Corleone is a very famous and respected person in the underworld, living according to old laws and traditions. But time goes on, times change, and one day Corleone is offered to deviate from his principles and engage in drug trafficking. Of course, he refuses, and soon an attempt is made on him ...'),
(3, 2, 'Крестный отец', 
'Культовая картина снята по одноименному роману американского писателя Марио Пьюзо. На дворе 1945 год. В центре сюжета сицилийская мафиозная семья Корлеоне, которая имеет жёсткие криминальные позиции в Нью-Йорке. Глава семейства Вито Корлеоне - очень известный и уважаемый человек в преступном мире, живущий по старым законам и традициям. Но время идет, времена меняются, и однажды Корлеоне предлагают отойти от его принципов, и заняться наркобизнесом. Само собой он отказывается, и в скором времени на него совершают покушение...'),
(4, 1, 'Léon: The Professional', 
'Leon leads a quiet and relaxed lifestyle in one of the not very best areas of New York called Little Italy. No one can even imagine that this man is a real contract killer. Once, while at home, Leon hears a noise, and, looking through the peephole of his door, he becomes a witness of how the whole family living in the next apartment is being killed. Only the teenage girl Matilda, who was not at home at the time, manages to survive. She turns to her neighbor Leon for help, because she has nowhere else to go. The cold-blooded killer suddenly begins to experience feelings previously alien to him, and lets the girl in. They begin to live together, and after learning how her savior makes a living, Matilda asks to teach her how to kill so that she can take revenge on the murderers of her family. But this is unlikely to lead to anything good ...'),
(4, 2, 'Леон', 
'Леон ведет тихий и спокойный образ жизни в одном из не самых лучших районов Нью-Йорка под названием Маленькая Италия. Никто даже представить себе не может, что этот человек самый настоящий наемный убийца. Однажды, находясь дома, Леон слышит шум, и, посмотрев в глазок своей двери, он становится свидетелем того, как всю семью, живущую в соседней квартире, убивают. Выжить удается только девочке-подростку Матильде, которой в то время не было дома. Она обращается к своему соседу Леону за помощью, ведь ей больше некуда идти. Хладнокровный убийца вдруг начинает испытывать чуждые ранее ему чувства, и пускает девочку к себе. Они начинают жить вместе, и узнав чем ее спаситель зарабатывает на жизнь, Матильда просит научить ее убивать, чтобы она могла отомстить убийцам своей семьи. Но вряд ли это приведет к чему-нибудь хорошему…'),
(5, 1, 'Parasite', 
'At the center of the story is the poor Kim family, made up of an unemployed driver, Gi-Taek, his wife Chun-Suk, Gi-Woo''s son, and Gi-Chon''s daughter. They live in a dilapidated basement apartment and try to make ends meet by collecting cardboard pizza boxes. One day Gi-Woo gets the opportunity to work as an English tutor for a girl from a wealthy Pak family. With the help of a fake diploma made by his sister, the guy successfully passes the interview and, fascinated by the luxurious life of these people, comes up with an unusual way to solve the financial problems of his relatives ...'),
(5, 2, 'Паразиты', 
'В центре истории находится бедное семейство Ким, состоящее из безработного водителя Ги-Тэка, его жены Чхун-Сук, сына Ги-У и дочери Ги-Чон. Они живут в ветхой полуподвальной квартире и пытаются свести концы с концами, собирая картонные коробки для пиццы. Однажды Ги-У получает возможность поработать репетитором английского языка для девушки из богатой семьи Пак. С помощью поддельного диплома, изготовленного сестрой, парень успешно проходит собеседование и, очарованный роскошной жизнью этих людей, придумывает необычный способ решения финансовых проблем своих родственников...');

INSERT INTO `cinema`.`cinema_hall`(size)
VALUES (52);

INSERT INTO `cinema`.`cinema_hall_translate`(cinema_hall_id, language_id, name)
VALUES(1, 1, 'coliseum'),
(1, 2, 'колизей');

INSERT INTO cinema.offer(film_id, cinema_hall_id, date, time, price)
VALUES 
(1, 1, '2020-09-25', '15:15:00', 120),
(2, 1, '2020-09-26', '19:30:00', 150),
(3, 1, '2020-09-27', '21:20:00', 140),
(4, 1, '2020-09-28', '15:00:00', 110),
(5, 1, '2020-09-29', '15:00:00', 110),
(1, 1, '2020-09-30', '20:20:00', 180),
(4, 1, '2020-09-30', '19:30:00', 120);

/*
SELECT * FROM cinema.offer_localized_with_orders O 
WHERE O.language_name = 'en'
AND O.date >= current_date() 
AND O.date <= current_date() + 30 
ORDER BY O.'date'
LIMIT 0, 4;

SELECT * FROM `film`;
SELECT * FROM `language`;
SELECT * FROM `film_translate`;
SELECT * FROM `cinema_hall`;
SELECT * FROM `cinema_hall_translate`;
SELECT * FROM `cinema`.`offer`;
SELECT * FROM cinema.system_role;
SELECT * FROM `cinema`.`offer`;
SELECT * FROM `cinema`.`film`;
*/