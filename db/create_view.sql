USE `cinema`;

/*
CREATE VIEW film_localized AS
SELECT F.id AS film_id, F.original_name AS original_film_name, F.imdb_rating AS film_imdb_rating, F.timekeeping AS film_timekeeping, F.image_dir AS film_poster_image_dir, FT.language_id, L.name AS language_name, FT.name AS localized_film_name, FT.description AS localized_film_description FROM cinema.film F
	JOIN cinema.film_translate FT
		ON F.id = FT.film_id
    JOIN cinema.language L
		ON FT.language_id = L.id;
    
SELECT * FROM cinema.film_localized;

CREATE VIEW cinema_hall_localized AS
SELECT HT.cinema_hall_id AS hall_id, H.size AS hall_size, H.image_dir AS hall_scheme_image_dir, HT.language_id, L.name AS language_name, HT.name AS localized_hall_name FROM cinema.cinema_hall H
	JOIN cinema.cinema_hall_translate HT
		ON H.id = HT.cinema_hall_id
    JOIN cinema.language L
		ON HT.language_id = L.id;
    
SELECT * FROM cinema.cinema_hall_localized;

CREATE VIEW offer_localized AS 
SELECT O.id AS offer_id, O.date, O.time, O.price, 
F.language_id, F.language_name, F.film_id, F.original_film_name, F.film_timekeeping, F.film_poster_image_dir, F.localized_film_name, F.localized_film_description,
H.hall_id, H.hall_size, H.hall_scheme_image_dir, H.localized_hall_name FROM OFFER O
	JOIN cinema.film_localized F
		ON O.film_id = F.film_id
	JOIN cinema.cinema_hall_localized H
		ON F.language_id = H.language_id;
        
SELECT * FROM cinema.offer_localized OL
WHERE
	OL.language_id = 1
	AND OL.date >= curdate()
    AND OL.date <= curdate() + 31;

CREATE VIEW offer_localized_with_orders AS
SELECT
OL.offer_id, OL.date, OL.time, OL.price, 
OL.language_id, OL.language_name, OL.film_id, OL.original_film_name, OL.film_timekeeping, OL.film_poster_image_dir, OL.localized_film_name, OL.localized_film_description,
OL.hall_id, OL.hall_size, OL.hall_scheme_image_dir, OL.localized_hall_name,
 COUNT(OD.id) ordered_units
FROM cinema.order OD
	RIGHT OUTER JOIN cinema.offer_localized OL
     ON OD.ticket_id = OL.offer_id 
GROUP BY OL.offer_id, OL.language_id


CREATE VIEW user_data AS
SELECT U.id as user_id, U.login, U.password, U.nickname, U.system_role_id, SR.name  
FROM cinema.user U
JOIN cinema.system_role SR
	ON U.system_role_id = SR.id;
    
SELECT * FROM cinema.user;

SELECT * FROM cinema.user_data;

SELECT * FROM cinema.offer_localized_with_orders;

SELECT OL.localized_film_description FROM cinema.offer_localized_with_orders OL
WHERE OL.offer_id = 2;

SELECT O.place FROM cinema.order O
WHERE O.ticket_id = 1;

CREATE VIEW order_localized AS
SELECT O.id AS ticket_id, O.user_id, O.place,
OL.offer_id, OL.date, OL.time, OL.localized_hall_name, OL.localized_film_name, OL.price, OL.language_id, OL.language_name
FROM cinema.order O
JOIN cinema.offer_localized OL
	ON O.ticket_id = OL.offer_id;
	

SELECT * FROM cinema.order_localized
WHERE user_id = 1
AND language_name = 'en';

SELECT * FROM cinema.film_localized FL
WHERE FL.language_name = 'en';

SELECT * FROM cinema.offer;

INSERT INTO cinema.offer(film_id, cinema_hall_id, date, time, price)
VALUES(5, 1, '2021-10-10', '10:10:10', 120);
*/
SELECT * FROM cinema.order;

INSERT INTO cinema.order(ticket_id, user_id, place, is_paid)
VALUES(4, 1, 1 , true);

SELECT * FROM cinema.user;

INSERT INTO cinema.user(login, password, nickname, system_role_id)
VALUES(?, ?, ?, ?);

SELECT * FROM cinema.offer_localized_with_orders O
WHERE 
O.language_name = 'en'
AND O.date >= current_date()
AND O.date <= current_date() + 31
AND O.film_id IN ('1','2','3')
ORDER BY ordered_units
LIMIT 0, 2;

SELECT F.film_id, F.localized_film_name FROM cinema.film_localized F
WHERE F.language_name = 'en'
GROUP BY F.film_id;

SELECT * FROM cinema.offer_localized_with_orders O 
WHERE O.language_name = 'en' 
AND O.date >= current_date() 
AND O.date <= current_date() + 1
AND offer_id IN ('3, 1')
ORDER BY 'ordered_units' LIMIT 0, 4;

