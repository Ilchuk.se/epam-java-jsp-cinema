package classes.com.my.controllers;

import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.dbmanager.viewmanagers.entity.UserData;
import classes.com.my.tools.localizer.Localizer;
import classes.com.my.tools.localizer.LocalizerException;
import classes.com.my.tools.navigation.PageRouter;

public abstract class BaseController {	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ControllerException{
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ControllerException {
	}
		
	protected final Locale getSessionLocale(HttpSession session) {
		Locale locale = (Locale)session.getAttribute(GlobalAttributes.SESSION_ATTR_LOCALE);
		if(locale == null) {
			locale = new Locale(GlobalAttributes.LOCALE_DEFFAULT);
		}
		return locale;
	}
	
	protected final Localizer getSessioLocalizer(HttpSession session) throws LocalizerException {
		Localizer localizer = (Localizer) session.getAttribute(GlobalAttributes.SESSION_LOCALIZER);
		if(localizer == null) {
			Locale locale = getSessionLocale(session);
			localizer = new Localizer(GlobalAttributes.LOCALE_BUNDLE, locale);
		}
		return localizer;
	}
	
	protected final void goForward(HttpServletRequest request, HttpServletResponse response, String innerPageAdress) throws ControllerException{
		try {
			request.setAttribute(GlobalAttributes.REQUEST_ATTR_INNER_PAGE, innerPageAdress);
			RequestDispatcher dispatcher = request.getRequestDispatcher(PageRouter.TEMPLATE.getPath());
			dispatcher.forward(request, response);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
	}
	
	protected final void goPreviousPage(HttpServletRequest request, HttpServletResponse response) throws ControllerException{
		try {
			String prevPageURL = request.getHeader("referer");
			response.sendRedirect(prevPageURL);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
	}
	
	protected final void sendRedirect(HttpServletRequest request, HttpServletResponse response, String URL) throws ControllerException {
		try {
			response.sendRedirect(request.getContextPath() + URL);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
	}
	
	protected final UserData getCurrentUser(HttpSession session) throws NullPointerException {
		UserData user = (UserData)session.getAttribute(GlobalAttributes.SESSION_ATTR_CUR_USER);
		return user;
	}
}
