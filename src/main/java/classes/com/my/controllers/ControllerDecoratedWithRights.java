package classes.com.my.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.dbmanager.viewmanagers.entity.UserData;

public class ControllerDecoratedWithRights {
	private final BaseController controller;
	private final List<UserRights> usersWithAccess;
	
	public ControllerDecoratedWithRights(BaseController controller, List<UserRights> usersWithAccess) {
		super();
		this.controller = controller;
		this.usersWithAccess = usersWithAccess;
	}
	
	public void forwardToController(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		HttpSession session = request.getSession();
		
		UserData user = (UserData) session.getAttribute(GlobalAttributes.SESSION_ATTR_CUR_USER);
		if(isUserHasRights(user)) {
			String method = request.getMethod().toLowerCase();
			
			if (method.equals("get")) {
				controller.doGet(request, response);
				return;
			} else if (method.equals("post")) {
				controller.doPost(request, response);
				return;
			}
		}else {
			throw new ControllerException("You don`t have access to this page. Please, enter account with grater access.", null);
		}
	}
	
	private boolean isUserHasRights(UserData user) {
		if(usersWithAccess.contains(UserRights.ALL)) {
			return true;
		}
		
		if(user == null) {
			return usersWithAccess.contains(UserRights.NOT_AUTHORIZED);
		}
		
		return usersWithAccess.stream().filter(x -> user.getSystemRoleId() == x.getId()).findFirst().isPresent();
	}
}
