package classes.com.my.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import classes.com.my.controllers.elements.footer.LanguageSelectController;
import classes.com.my.controllers.page.ControllerManager;
import classes.com.my.controllers.page.authorization.AuthorizarionPageController;
import classes.com.my.controllers.page.cart.OrdersPageController;
import classes.com.my.controllers.page.error.ErrorPageController;
import classes.com.my.controllers.page.filmdescription.FilmOfferPageController;
import classes.com.my.controllers.page.main.MainPageController;
import classes.com.my.controllers.page.profile.ProfilePageController;
import classes.com.my.controllers.page.profile.elements.ProfileAddOfferPageController;
import classes.com.my.controllers.page.profile.elements.ProfileInfoPageController;
import classes.com.my.controllers.page.profile.elements.ProfileOfferPageController;
import classes.com.my.controllers.page.profile.elements.ProfileStatisticsPageController;
import classes.com.my.controllers.page.registration.RegistrationPageController;
import classes.com.my.tools.navigation.PageRouter;

public class FrontFilter implements Filter {
	private static final Logger log = LogManager.getLogger(FrontFilter.class);

	public static final String REQUEST_ATTR_EXCEPTION = "mainFilterExcp";
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		
		try {	
			ControllerDecoratedWithRights pageController = getPageController(httpRequest);
			
			if(pageController != null) {
				pageController.forwardToController(httpRequest, httpResponse);
				return;
			}
			
			redirectToMainPage(httpRequest, httpResponse);
		} catch (Exception ex) {			
			log.warn("Global handler exception occured, with controller redirection.", ex);
			
			request.setAttribute(REQUEST_ATTR_EXCEPTION, ex.getMessage());
			
			BaseController errorPageController = new ErrorPageController();
			try {
				forwardToConroller(errorPageController, httpRequest, httpResponse);
			} catch (Exception ex2) {
				log.error("Page redirection didn`t work", ex2);
				httpResponse.sendError(0, "Something goes wrong, pleas access the developer. Num: +some number");
			}
		}
		
	}
	
	private final ControllerDecoratedWithRights getPageController(HttpServletRequest request) {
		String pageURL = request.getServletPath();		
		return ControllerManager.URL_TO_CONTROLLER.get(pageURL);
	}

	private final void redirectToMainPage(HttpServletRequest request, HttpServletResponse response) throws IOException, IllegalStateException {
		String url = request.getRequestURL().toString();
		String path = request.getServletPath();
		
		String root = url.replace(path, "");
		response.sendRedirect(root + PageRouter.MAIN.getURL());
	}
	
	private final synchronized void forwardToConroller(BaseController controller, HttpServletRequest request,
		HttpServletResponse response) throws ControllerException {
		
		String method = request.getMethod().toLowerCase();

		if (method.equals("get")) {
			controller.doGet(request, response);
			return;
		} else if (method.equals("post")) {
			controller.doPost(request, response);
			return;
		}
	}
	
	@Override
	public void destroy() {
	}
}
