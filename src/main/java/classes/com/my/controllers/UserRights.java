package classes.com.my.controllers;

public enum UserRights {
	ALL(-1) ,NOT_AUTHORIZED(null), USER(1), ADMIN(2);
	
	private final Integer id;

	public Integer getId() {
		return id;
	
	}

	private UserRights(Integer id) {
		this.id = id;
	}
}
