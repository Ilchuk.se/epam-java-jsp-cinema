package classes.com.my.controllers.elements.mainpage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classes.com.my.controllers.elements.IViewActionElementContainer;
import classes.com.my.controllers.elements.ViewActionElement;
import classes.com.my.tools.localizer.Localizer;

public class FilmSortingByTypesAEContainer extends IViewActionElementContainer {
	private static final Map<String, String> propFileKeys;
	
	static {
		propFileKeys = new HashMap<>();
		propFileKeys.put(SortOffer.NAME.getId(), "main_page_sort_by_name");
		propFileKeys.put(SortOffer.DATE.getId(), "main_page_sort_by_date");
		propFileKeys.put(SortOffer.PLACES.getId(), "main_page_sort_by_places");
	}

	public List<ViewActionElement> getAllElements(Localizer loc) {
		return super.getAllElementsString(loc, propFileKeys);
	}
}
