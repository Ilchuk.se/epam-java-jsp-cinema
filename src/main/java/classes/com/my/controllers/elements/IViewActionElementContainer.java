package classes.com.my.controllers.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import classes.com.my.tools.localizer.Localizer;

public abstract class IViewActionElementContainer {	
	protected List<ViewActionElement> getAllElements(Localizer loc, Map<Integer, String> propFileKeys){
		List<ViewActionElement> Elements = new ArrayList<>();
		try {
			for (Map.Entry<Integer, String> prop: propFileKeys.entrySet()) {
				Elements.add(
					new ViewActionElement(
						String.valueOf(prop.getKey()), 
						loc.getValue(prop.getValue())
					)
				);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return Elements;
	};
	
	protected List<ViewActionElement> getAllElementsString(Localizer loc, Map<String, String> propFileKeys){
		List<ViewActionElement> Elements = new ArrayList<>();
		try {
			for (Map.Entry<String, String> prop: propFileKeys.entrySet()) {
				Elements.add(
					new ViewActionElement(
						prop.getKey(), 
						loc.getValue(prop.getValue())
					)
				);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return Elements;
	};
}
