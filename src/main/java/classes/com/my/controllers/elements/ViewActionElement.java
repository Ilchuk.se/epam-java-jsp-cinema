package classes.com.my.controllers.elements;

public class ViewActionElement {
	private String label;
	private String value;
	
	public ViewActionElement(String value, String label) {
		super();
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "[val = " + value + ", lab = " + label + "]";
	}

	@Override
	public int hashCode() {
		return this.getValue().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj instanceof ViewActionElement) {
			if(this.getValue().equals(((ViewActionElement)obj).getValue())) {
				return true;
			}
		}
		return false;
	}
}
