package classes.com.my.controllers.elements.mainpage;

public enum FilterOfferByDate {
	TODAY("1", "0"), 
	TOMORROW("2" , "1"), 
	WEAK("3", "6"), 
	MONTH("4", "30");
	
	private final String id;
	private final String days;
	
	private FilterOfferByDate(String id, String days) {
		this.id = id;
		this.days = days;
	}
	
	public static FilterOfferByDate findById(String id) {
		if(id == null) {
			return FilterOfferByDate.MONTH;
		}
		
		for(FilterOfferByDate filter : values()) {
			if(filter.getId().equals(id)) {
				return filter;
			}
		}
		
		return FilterOfferByDate.MONTH;
	}
	
	public String getId() {
		return id;
	}
	
	public String getDays() {
		return days;
	}
}
