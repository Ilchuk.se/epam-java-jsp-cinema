package classes.com.my.controllers.elements.mainpage;

public enum SortOffer {
	NAME("1", "localized_film_name"), 
	DATE("2", "date"), 
	PLACES("3", "ordered_units");
	
	private final String id;
	private final String SQLCommand;
	
	private SortOffer(String id, String SQLCommand) {
		this.id = id;
		this.SQLCommand = SQLCommand;
	}
	
	public static SortOffer findById(String id) {
		if(id == null) {
			return SortOffer.PLACES;
		}
		
		for(SortOffer sort : values()) {
			if(sort.getId().equals(id)) {
				return sort;
			}
		}
		
		return SortOffer.PLACES;
	}
	
	public String getId() {
		return id;
	}
	public String getSQLCommand() {
		return SQLCommand;
	}
}
