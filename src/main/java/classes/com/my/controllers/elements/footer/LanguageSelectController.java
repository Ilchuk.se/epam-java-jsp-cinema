package classes.com.my.controllers.elements.footer;


import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.localizer.Localizer;


public class LanguageSelectController extends BaseController {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		String lang = (String)request.getParameter(GlobalAttributes.REQUEST_ATTR_LANG);  
		if(lang == null) {
			return;
		}
		
		try {
			Locale locale = new Locale(lang);
			Localizer localizer = new Localizer(GlobalAttributes.LOCALE_BUNDLE, locale);
			
			HttpSession session = request.getSession();
			
			session.setAttribute(GlobalAttributes.SESSION_ATTR_LOCALE, locale);
			session.setAttribute(GlobalAttributes.SESSION_LOCALIZER, localizer);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
		super.goPreviousPage(request, response);
	}
}
