package classes.com.my.controllers.elements.footer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classes.com.my.controllers.elements.IViewActionElementContainer;
import classes.com.my.controllers.elements.ViewActionElement;
import classes.com.my.tools.localizer.Localizer;

public class LangSelectAEContainer extends IViewActionElementContainer{
private static final Map<String, String> propFileKeys;
	
	static {
		propFileKeys = new HashMap<>();
		propFileKeys.put("en", "footer_option_eng");
		propFileKeys.put("ru", "footer_option_ru");
	}
	
	public List<ViewActionElement> getAllElements(Localizer loc) {
		return super.getAllElementsString(loc, propFileKeys);
	}
}
