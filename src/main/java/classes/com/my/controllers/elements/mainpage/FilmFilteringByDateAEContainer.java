package classes.com.my.controllers.elements.mainpage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classes.com.my.controllers.elements.IViewActionElementContainer;
import classes.com.my.controllers.elements.ViewActionElement;
import classes.com.my.tools.localizer.Localizer;

public class FilmFilteringByDateAEContainer extends IViewActionElementContainer{
	private static final Map<String, String> propFileKeys;
	
	static {
		propFileKeys = new HashMap<>();
		propFileKeys.put(FilterOfferByDate.TODAY.getId() , "main_page_filter_by_date_today");
		propFileKeys.put(FilterOfferByDate.TOMORROW.getId(), "main_page_filter_by_date_tomorrow");
		propFileKeys.put(FilterOfferByDate.WEAK.getId(), "main_page_filter_by_date_weak");
		propFileKeys.put(FilterOfferByDate.MONTH.getId(), "main_page_filter_by_date_month");
	}
	
	public List<ViewActionElement> getAllElements(Localizer loc) {
		return super.getAllElementsString(loc, propFileKeys);
	}
}
