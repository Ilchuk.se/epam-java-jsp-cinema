package classes.com.my.controllers.page.cart;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.dbmanager.viewmanagers.OrderLocalizedManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.OrderLocalized;
import classes.com.my.tools.dbmanager.viewmanagers.entity.UserData;
import classes.com.my.tools.navigation.PageRouter;

public class OrdersPageController extends BaseController {
	public static final String REQUEST_ATTR_CUR_USER_ORDER = "curUserOrders";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		HttpSession session = request.getSession();
		
		UserData curUser = (UserData) session.getAttribute(GlobalAttributes.SESSION_ATTR_CUR_USER);
		Locale locale = (Locale) session.getAttribute(GlobalAttributes.SESSION_ATTR_LOCALE);
		if(curUser == null || locale == null) {
			super.goPreviousPage(request, response);
		}
		
		List<OrderLocalized> curUserOrders = OrderLocalizedManager.getAllOrdersLocalizedById(locale, String.valueOf(curUser.getUserId()));
		curUserOrders.sort(Comparator.comparing(OrderLocalized::getDate).thenComparing(OrderLocalized::getTime));
		System.out.println(curUserOrders.toString());
		
		request.setAttribute(REQUEST_ATTR_CUR_USER_ORDER, curUserOrders);
		
		goForward(request, response, PageRouter.ORDERS.getPath());
	}
}
