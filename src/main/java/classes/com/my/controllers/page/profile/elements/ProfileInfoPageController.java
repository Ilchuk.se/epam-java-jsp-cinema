package classes.com.my.controllers.page.profile.elements;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.controllers.page.profile.ProfilePageController;
import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.navigation.PageRouter;

public class ProfileInfoPageController extends BaseController {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {		
		ProfilePageController.setInnerElement(request, PageRouter.PROFILE_INFO.getPath());
		super.goForward(request, response, PageRouter.PROFILE.getPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		HttpSession session = request.getSession();
		session.removeAttribute(GlobalAttributes.SESSION_ATTR_CUR_USER);
		super.sendRedirect(request, response, PageRouter.MAIN.getURL());
	}
}
