package classes.com.my.controllers.page.profile.elements;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.controllers.page.profile.ProfilePageController;
import classes.com.my.tools.dbmanager.viewmanagers.OfferLocalizedManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.OfferLocalized;
import classes.com.my.tools.navigation.PageRouter;

public class ProfileOfferPageController extends BaseController {
	public static final String REQUEST_ATTR_ALL_OFFERS = "allOff"; 
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {
			HttpSession session = request.getSession();
			Locale locale = super.getSessionLocale(session);
			
			List<OfferLocalized> offersList = OfferLocalizedManager.getAllOffersLocalized(locale);
			request.setAttribute(REQUEST_ATTR_ALL_OFFERS, offersList);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
		ProfilePageController.setInnerElement(request, PageRouter.PROFILE_OFFER.getPath());
		super.goForward(request, response, PageRouter.PROFILE.getPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		doGet(request, response);
	}
}
