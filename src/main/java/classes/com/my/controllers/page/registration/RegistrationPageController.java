package classes.com.my.controllers.page.registration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.dbmanager.tablemanagers.UserManager;
import classes.com.my.tools.dbmanager.tablemanagers.entity.User;
import classes.com.my.tools.dbmanager.viewmanagers.UserDataManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.UserData;
import classes.com.my.tools.hashing.MD5;
import classes.com.my.tools.navigation.PageRouter;

public class RegistrationPageController extends BaseController {
	private static final long serialVersionUID = 1L;
	
	public static final String REQUEST_ATTR_MESSAGE = "regPageMessage";
	
	public static final String REGUEST_POST_ATTR_NAME = "regPageName";
	public static final String REGUEST_POST_ATTR_LOGIN = "regPageLogin";
	public static final String REGUEST_POST_ATTR_PASSWORD = "regPagePass";
	public static final String REGUEST_POST_ATTR_PASSWORD_DUP = "regPagePassDup";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {		
		super.goForward(request, response, PageRouter.REGISTRATION.getPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		final String name = request.getParameter(RegistrationPageController.REGUEST_POST_ATTR_NAME);
		final String login = request.getParameter(RegistrationPageController.REGUEST_POST_ATTR_LOGIN);
		final String password = request.getParameter(RegistrationPageController.REGUEST_POST_ATTR_PASSWORD);
		final String passwordDup = request.getParameter(RegistrationPageController.REGUEST_POST_ATTR_PASSWORD_DUP);
		
		try {
			if(name == null || name.length() < 6) {
				throw new IllegalArgumentException("Name length must be at leas 6 symblos.");
			}
			if(login == null || login.length() < 4) {
				throw new IllegalArgumentException("Login length must be at least 4 symblos.");
			}
			if(password == null || login.length() < 4) {
				throw new IllegalArgumentException("Password length must be at least 4 symblos.");
			}
			if(passwordDup == null || !password.equals(passwordDup)) {
				throw new IllegalArgumentException("Mismatch between password and it`s duplication.");
			}
			
			User user = new User();
			user.setName(name);
			user.setLogin(login);
			user.setPassword(MD5.getHash(password));
			user.setRoleId(1);
		
			UserManager.addNewUser(user);
			UserData userData = UserDataManager.getAuthorizedUser(login, password);
			if(userData != null);
			
			HttpSession session = request.getSession();
			session.setAttribute(GlobalAttributes.SESSION_ATTR_CUR_USER, userData);
		} catch (Exception ex) {
			ex.printStackTrace();
			request.setAttribute(REQUEST_ATTR_MESSAGE, ex.getMessage());
			doGet(request, response);
			return;
		}
		
		super.sendRedirect(request, response, PageRouter.MAIN.getURL());
	}
}
