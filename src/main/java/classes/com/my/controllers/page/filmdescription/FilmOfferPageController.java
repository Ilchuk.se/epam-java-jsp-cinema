package classes.com.my.controllers.page.filmdescription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.tools.dbmanager.DBManagerException;
import classes.com.my.tools.dbmanager.tablemanagers.OrderManager;
import classes.com.my.tools.dbmanager.tablemanagers.entity.Order;
import classes.com.my.tools.dbmanager.viewmanagers.OfferLocalizedManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.OfferLocalized;
import classes.com.my.tools.dbmanager.viewmanagers.entity.UserData;
import classes.com.my.tools.navigation.PageRouter;



public class FilmOfferPageController extends BaseController {
	public static final String REQUEST_ATTR_FILM_ID = "id";
	public static final String REQUEST_ATTR_FILM = "film";
	public static final String REQUEST_ATTR_BOOCKED_PLACES = "boockedPlaces";
	public static final String REQUEST_ATTR_IS_OFFER_DONE = "offerClosed";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {
			String filmID = request.getParameter(REQUEST_ATTR_FILM_ID);
			if(filmID == null) {
				throw new IllegalArgumentException("Film was not selected");
			}
			
			HttpSession session = request.getSession();
			Locale locale = super.getSessionLocale(session);
			
			OfferLocalized film = OfferLocalizedManager.getOfferLocalizedById(locale, filmID);
			if(film == null) {
				throw new IllegalArgumentException("No such film exisits.");
			}
			
			final long millis=System.currentTimeMillis();  
			final java.sql.Date date =new java.sql.Date(millis);
			final int compareDate = film.getOfferDate().compareTo(date);
			if(compareDate >= 0) {
				request.setAttribute(REQUEST_ATTR_IS_OFFER_DONE, true);
			}
			
			request.setAttribute(REQUEST_ATTR_FILM, film);
			
			List<Integer> boockedPlaces = OrderManager.getAllBookedPlacesByOfferID(filmID);
			request.setAttribute(REQUEST_ATTR_BOOCKED_PLACES, boockedPlaces);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
		super.goForward(request, response, PageRouter.FILM_DESCRIPTION.getPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {
			HttpSession session = request.getSession();
			UserData user = super.getCurrentUser(session);
			
			if(user == null) {
				throw new IllegalArgumentException("Please, sign in.");
			}
			
			String filmIDString = request.getParameter(REQUEST_ATTR_FILM_ID);
			if(filmIDString == null) {
				throw new IllegalArgumentException("Film was not selected.");
			}
			
			int filmID = Integer.parseInt(filmIDString);
			
			String[] placesArr = request.getParameterValues("sp[]");
			if(placesArr == null) {
				throw new IllegalArgumentException("Please, select place.");
			}
			
			List<String> places = Arrays.asList(placesArr);
			formAndSendOrder(user, places, filmID);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
		doGet(request, response);
	}
	
	private static final void formAndSendOrder(UserData user, List<String> places, int ticketId) throws DBManagerException, IllegalArgumentException {
		List<Order> orders = new ArrayList<>();
		
		for (String placeString : places) {
			int place = Integer.parseInt(placeString);
			if(place < 1) {
				throw new IllegalArgumentException("Place number can not be less than 1, but was " + place);
			}
			
			Order order = new Order();
			order.setPaid(true);
			order.setPlace(place);
			order.setUserId(user.getUserId());
			order.setTicketId(ticketId);
			
			orders.add(order);
		}
		
		OrderManager.addNewOrders(orders);
	}
}
