package classes.com.my.controllers.page.error;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.tools.navigation.PageRouter;

public class ErrorPageController extends BaseController{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		super.goForward(request, response, PageRouter.ERROR.getPath());
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		super.goForward(request, response, PageRouter.ERROR.getPath());
	}
}
