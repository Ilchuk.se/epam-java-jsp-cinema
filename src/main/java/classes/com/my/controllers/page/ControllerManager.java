package classes.com.my.controllers.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classes.com.my.controllers.ControllerDecoratedWithRights;
import classes.com.my.controllers.UserRights;
import classes.com.my.controllers.elements.footer.LanguageSelectController;
import classes.com.my.controllers.page.authorization.AuthorizarionPageController;
import classes.com.my.controllers.page.cart.OrdersPageController;
import classes.com.my.controllers.page.filmdescription.FilmOfferPageController;
import classes.com.my.controllers.page.main.MainPageController;
import classes.com.my.controllers.page.profile.ProfilePageController;
import classes.com.my.controllers.page.profile.elements.ProfileAddOfferPageController;
import classes.com.my.controllers.page.profile.elements.ProfileInfoPageController;
import classes.com.my.controllers.page.profile.elements.ProfileOfferPageController;
import classes.com.my.controllers.page.profile.elements.ProfileStatisticsPageController;
import classes.com.my.controllers.page.registration.RegistrationPageController;
import classes.com.my.tools.navigation.PageRouter;

public class ControllerManager {
	public static final Map<String, ControllerDecoratedWithRights> URL_TO_CONTROLLER;
	static {
		MainPageController mainPageController = new MainPageController();
		
		LanguageSelectController languageSelectController = new LanguageSelectController();
		
		RegistrationPageController registrationPageController = new RegistrationPageController();
		AuthorizarionPageController authorizarionPageController = new AuthorizarionPageController();
		
		OrdersPageController ordersPageController = new OrdersPageController();
		FilmOfferPageController filmOfferPageController = new FilmOfferPageController();
		
		ProfilePageController profilePageController = new ProfilePageController();
		ProfileInfoPageController profileInfoPageController = new ProfileInfoPageController();
		ProfileOfferPageController profileOfferPageController = new ProfileOfferPageController();
		ProfileAddOfferPageController profileAddOfferPageController = new ProfileAddOfferPageController();
		ProfileStatisticsPageController profileStatisticsPageController = new ProfileStatisticsPageController();
		
		URL_TO_CONTROLLER = new HashMap<>();
		
		List<UserRights> userRights = new ArrayList<UserRights>(1);
		userRights.add(UserRights.ALL);
		
		//Pages for all users 
		ControllerDecoratedWithRights manager = new ControllerDecoratedWithRights(mainPageController, userRights);
		URL_TO_CONTROLLER.put("", manager);
		URL_TO_CONTROLLER.put(PageRouter.MAIN.getURL(), manager);
		
		manager = new ControllerDecoratedWithRights(languageSelectController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.FOOTER.getURL(), manager);
		
		manager = new ControllerDecoratedWithRights(filmOfferPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.FILM_DESCRIPTION.getURL(), manager);
		
		//Pages for not authorized user
		userRights = new ArrayList<UserRights>(1);
		userRights.add(UserRights.NOT_AUTHORIZED);
		
		manager = new ControllerDecoratedWithRights(registrationPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.REGISTRATION.getURL(), manager);
		
		manager = new ControllerDecoratedWithRights(authorizarionPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.AUTHORISATION.getURL(), manager);
		
		//Pages for user and admin
		userRights = new ArrayList<UserRights>(1);
		userRights.add(UserRights.ADMIN);
		
		manager = new ControllerDecoratedWithRights(profileOfferPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.PROFILE_OFFER.getURL(), manager);
		
		manager = new ControllerDecoratedWithRights(profileAddOfferPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.PROFILE_OFFER_NEW.getURL(), manager);
		
		manager = new ControllerDecoratedWithRights(profileStatisticsPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.PROFILE_STATISTICS.getURL(), manager);
				
		//Pages for user and admin
		userRights = new ArrayList<UserRights>(2);
		userRights.add(UserRights.USER);
		userRights.add(UserRights.ADMIN);
		
		manager = new ControllerDecoratedWithRights(ordersPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.ORDERS.getURL(), manager);
		
		manager = new ControllerDecoratedWithRights(profilePageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.PROFILE.getURL(), manager);
		
		manager = new ControllerDecoratedWithRights(profileInfoPageController, userRights);
		URL_TO_CONTROLLER.put(PageRouter.PROFILE_INFO.getURL(), manager);
	}
}
