package classes.com.my.controllers.page.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.controllers.elements.ViewActionElement;
import classes.com.my.controllers.elements.mainpage.FilmFilteringByDateAEContainer;
import classes.com.my.controllers.elements.mainpage.FilmSortingByTypesAEContainer;
import classes.com.my.controllers.elements.mainpage.FilterOfferByDate;
import classes.com.my.controllers.elements.mainpage.SortOffer;
import classes.com.my.tools.dbmanager.DBParser;
import classes.com.my.tools.dbmanager.viewmanagers.FilmLocalizedManager;
import classes.com.my.tools.dbmanager.viewmanagers.OfferLocalizedManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.OfferLocalized;
import classes.com.my.tools.localizer.Localizer;
import classes.com.my.tools.navigation.PageRouter;

public class MainPageController extends BaseController {
	private static final long serialVersionUID = 1L;

	public static final String REQUEST_ATTR_OFFERS_DISPLAYED = "desplayedOffers";
	
	public static final String REQUEST_ATTR_VIEW_FILTER_FILMS_ALL = "allFilms";
	public static final String SESSION_ATTR_VIEW_FILTER_FILMS_SELECTED = "filterSelectedFilms";
	
	public static final String REQUEST_ATTR_VIEW_SORT_ALL = "sortElements";
	public static final String SESSION_ATTR_VIEW_SORT_SELECTED = "selectedSortElement";
	
	public static final String REQUEST_ATTR_VIEW_FILTER_DATE_ALL = "filterDateElements";
	public static final String SESSION_ATTR_VIEW_FILTER_DATE_SELECTED = "selectedFilterDateElement";
	
	public static final String REQUEST_GET_ATTR_SORT = "s";
	public static final String REQUEST_GET_ATTR_FILTER_DATE = "fd";
	public static final String REQUEST_GET_ATTR_FILTER_FILM = "ff[]";
	
	public static final String REQUEST_GET_ATTR_PAGE = "page";
	public static final String REQUEST_ATTR_PAGE_NUM = "pageNum";
	public static final String REQUEST_ATTR_ALL_PAGE_COUNT = "allPageCount";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {						
			HttpSession session = request.getSession();
			
			Locale locale = super.getSessionLocale(session);
			Localizer localizer = super.getSessioLocalizer(session);
			
			setSortVAEToRequest(request, localizer);
			SortOffer sortOffer = setSelectedSortOptionToSession(request, session);
			
			setFilterByDateVAEToRequest(request, localizer);
			FilterOfferByDate filterOfferByDate = setSelectedFilterByDateOptionToSession(request, session);
			
			setFilterByFilmVAEToRequset(request, locale);
			List<String> selectedFilmsStrings = setSelectedFilmFilterToSession(request, session);
			
			List<Integer> selectedFilms = selectedFilmsStrings.stream().map(Integer::valueOf).collect(Collectors.toList());
			
			configurePagination(request, locale, filterOfferByDate, sortOffer, selectedFilms);
			
			super.goForward(request, response, PageRouter.MAIN.getPath());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
	}
	
	private final void configurePagination(HttpServletRequest request, Locale locale, FilterOfferByDate filter, SortOffer sort, List<Integer> selectedFilms) {
		String pageString = request.getParameter(REQUEST_GET_ATTR_PAGE);
		if(pageString == null || !DBParser.tryParseInt(pageString)) {
			pageString = "1";
		}
		
		final int productsOnPage = 6;
		
		int offersNum = OfferLocalizedManager.getNumberOfFilteredOfferLocalized(locale, filter, sort, selectedFilms);
		
		int tmpPageNum = offersNum / productsOnPage;
		if(offersNum % productsOnPage != 0) {
			tmpPageNum++;
		}
		
		final int allPagesNum = tmpPageNum;
		
		int pageNum = Integer.parseInt(pageString);
		if(pageNum < 1 || pageNum > allPagesNum) {
			pageNum = 1;
		}
		
		int firstIndex = (pageNum - 1) * productsOnPage;
				
		List<OfferLocalized> desplayedOffers = OfferLocalizedManager.getAllFilteredOfferLocalized(locale, filter, sort, selectedFilms, firstIndex, productsOnPage);
		request.setAttribute(REQUEST_ATTR_OFFERS_DISPLAYED, desplayedOffers);
		
		request.setAttribute(REQUEST_ATTR_PAGE_NUM, String.valueOf(pageNum));
		request.setAttribute(REQUEST_ATTR_ALL_PAGE_COUNT, allPagesNum);
	}
	
	private final void setSortVAEToRequest(HttpServletRequest request, Localizer localizer) {
		List<ViewActionElement> sortElems = new FilmSortingByTypesAEContainer().getAllElements(localizer);
		request.setAttribute(REQUEST_ATTR_VIEW_SORT_ALL, sortElems);
	}
	
	private final SortOffer setSelectedSortOptionToSession(HttpServletRequest request, HttpSession session) {
		String sortSelected = request.getParameter(REQUEST_GET_ATTR_SORT);
		String sessionSort = (String)session.getAttribute(SESSION_ATTR_VIEW_SORT_SELECTED);
		
		if(sortSelected == null) {
			sortSelected = sessionSort;
		}
		
		SortOffer sortOffer = SortOffer.findById(sortSelected);
		if(sortOffer == null) {
			session.setAttribute(SESSION_ATTR_VIEW_SORT_SELECTED, SortOffer.PLACES.getId());
		}else {
			session.setAttribute(SESSION_ATTR_VIEW_SORT_SELECTED, sortOffer.getId());
		}
		
		return sortOffer;
	}
	
	private final void setFilterByDateVAEToRequest(HttpServletRequest request, Localizer localizer) {
		List<ViewActionElement> filterDateElems = new FilmFilteringByDateAEContainer().getAllElements(localizer);
		request.setAttribute(REQUEST_ATTR_VIEW_FILTER_DATE_ALL, filterDateElems);
	}
	
	private final FilterOfferByDate setSelectedFilterByDateOptionToSession(HttpServletRequest request, HttpSession session) {
		String filterDateSelected =  request.getParameter(REQUEST_GET_ATTR_FILTER_DATE);
		String sessionDateFilter = (String)session.getAttribute(SESSION_ATTR_VIEW_FILTER_DATE_SELECTED);
		
		if(filterDateSelected == null) {
			filterDateSelected = sessionDateFilter;
		}
		
		FilterOfferByDate filterOfferByDate = FilterOfferByDate.findById(filterDateSelected);
		if(filterOfferByDate == null) {
			session.setAttribute(SESSION_ATTR_VIEW_FILTER_DATE_SELECTED, FilterOfferByDate.MONTH.getId());
		}else {
			session.setAttribute(SESSION_ATTR_VIEW_FILTER_DATE_SELECTED, filterDateSelected);
		}
		
		return filterOfferByDate;
	}
	
	private final void setFilterByFilmVAEToRequset(HttpServletRequest request, Locale locale) {
		List<ViewActionElement> filmFilters = FilmLocalizedManager.getFilmFilterViewAEList(locale);
		request.setAttribute(REQUEST_ATTR_VIEW_FILTER_FILMS_ALL, filmFilters);
	}
	
	private final List<String> setSelectedFilmFilterToSession(HttpServletRequest request, HttpSession session){
		String[] selectedFilmArr = request.getParameterValues(REQUEST_GET_ATTR_FILTER_FILM);
		List<String> sessionSelectedFilms = (List<String>) session.getAttribute(SESSION_ATTR_VIEW_FILTER_FILMS_SELECTED);
		List<String> selectedFilms = null;
		
		String selectedDate = request.getParameter(REQUEST_GET_ATTR_FILTER_DATE);
		String selectedSort = request.getParameter(REQUEST_GET_ATTR_SORT);
		String page = request.getParameter(REQUEST_GET_ATTR_PAGE);
		
		if(selectedDate != null || selectedSort != null || page != null) {
			return sessionSelectedFilms;
		}
				
		if(selectedFilmArr == null || selectedFilmArr.length == 0) {
			selectedFilms = new ArrayList<String>();
		}else {
			selectedFilms = Arrays.stream(selectedFilmArr).filter(x -> DBParser.tryParseInt(x)).collect(Collectors.toList());
		}
		
		session.setAttribute(SESSION_ATTR_VIEW_FILTER_FILMS_SELECTED, selectedFilms);
		return selectedFilms;
	}
}
