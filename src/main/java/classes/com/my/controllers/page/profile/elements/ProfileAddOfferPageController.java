package classes.com.my.controllers.page.profile.elements;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.controllers.page.profile.ProfilePageController;
import classes.com.my.tools.dbmanager.tablemanagers.OfferManager;
import classes.com.my.tools.dbmanager.tablemanagers.entity.Offer;
import classes.com.my.tools.dbmanager.viewmanagers.FilmLocalizedManager;
import classes.com.my.tools.dbmanager.viewmanagers.HallLocalizedManger;
import classes.com.my.tools.dbmanager.viewmanagers.entity.FilmLocalized;
import classes.com.my.tools.dbmanager.viewmanagers.entity.HallLocalized;
import classes.com.my.tools.navigation.PageRouter;

public class ProfileAddOfferPageController extends BaseController {
	public static final String REQUEST_ATTR_HALL_LOCALIZED_LIST = "listHallLoc";
	public static final String REQUEST_ATTR_FILM_LOCALIZED_LIST = "listFilmLoc";
	
	public static final String REQUEST_ATTR_HALL_ID = "hallId";
	public static final String REQUEST_ATTR_FILM_ID = "filmId";
	public static final String REQUEST_ATTR_TIME = "time";
	public static final String REQUEST_ATTR_DATE = "date";
	public static final String REQUEST_ATTR_PRICE = "price";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {
			HttpSession session = request.getSession();
			
			Locale locale = super.getSessionLocale(session);
			List<HallLocalized> hallList = HallLocalizedManger.ListgetAllHallLocalized(locale);
			request.setAttribute(REQUEST_ATTR_HALL_LOCALIZED_LIST, hallList);
			
			List<FilmLocalized> filmList = FilmLocalizedManager.getAllFilmLocalized(locale);
			request.setAttribute(REQUEST_ATTR_FILM_LOCALIZED_LIST, filmList);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
		ProfilePageController.setInnerElement(request, PageRouter.PROFILE_OFFER_NEW.getPath());
		super.goForward(request, response, PageRouter.PROFILE.getPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {
			String hallId = (String)request.getParameter(REQUEST_ATTR_HALL_ID);
			String filmId = (String)request.getParameter(REQUEST_ATTR_FILM_ID);
			String date = (String)request.getParameter(REQUEST_ATTR_DATE);
			String time = (String)request.getParameter(REQUEST_ATTR_TIME);
			time += ":00";
			String price = (String)request.getParameter(REQUEST_ATTR_PRICE);
			
			Offer offer = new Offer();
		
			offer.setHallId(Integer.parseInt(hallId));
			offer.setFilmId(Integer.parseInt(filmId));
			offer.setDate(Date.valueOf(date));
			offer.setTime(Time.valueOf(time));
			offer.setPrice(new BigDecimal(price));
			
			OfferManager.insertNewOffer(offer);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
		super.sendRedirect(request, response, PageRouter.PROFILE_OFFER.getURL());
	}
}
