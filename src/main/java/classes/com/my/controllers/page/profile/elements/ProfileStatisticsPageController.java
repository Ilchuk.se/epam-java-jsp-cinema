package classes.com.my.controllers.page.profile.elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.xml.simpleparser.NewLineHandler;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.controllers.page.profile.ProfilePageController;
import classes.com.my.tools.dbmanager.viewmanagers.OfferLocalizedManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.OfferLocalized;
import classes.com.my.tools.navigation.PageRouter;


public class ProfileStatisticsPageController extends BaseController {
	public static final String REQUEST_ATTR_ALL_OFFERS = "statAllOff";
	public static final String REQUEST_ATTR_PDF_FILE_NAME = "pdfFile";
	public static final String REQUEST_ATTR_HALL_STATS = "stats";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		HttpSession session = request.getSession();
		
		try {
			Locale locale = super.getSessionLocale(session);
			List<OfferLocalized> offers = OfferLocalizedManager.getAllOffersLocalized(locale);
			
			request.setAttribute(REQUEST_ATTR_ALL_OFFERS, offers);
			saveListOfferAsPDF(offers, request);
			request.setAttribute(REQUEST_ATTR_HALL_STATS, new HallStats(offers));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ControllerException(ex.getMessage(), ex);
		}
		
		ProfilePageController.setInnerElement(request, PageRouter.PROFILE_STATISTICS.getPath());
		super.goForward(request, response, PageRouter.PROFILE.getPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {
		//fetch data to URL
		String fileName = (String) request.getParameter(REQUEST_ATTR_PDF_FILE_NAME);
		final String path =  request.getRealPath("/");
		final String filePath = path + "\\" + fileName;
		
		File dwFile = new File(filePath);
		if(!dwFile.exists()) {
			throw new ControllerException("Sorry, we can not find your file.", null);
		}
		
		response.setContentType("application/octet-stream");
		response.setContentLength((int)dwFile.length());
		
		//force to download
		String hKey = "Content-Disposition";
		String hValue = String.format("attachment; filename=\"%s\"", dwFile.getName());
		
		response.setHeader(hKey, hValue);
		
		FileInputStream iStream = new FileInputStream(dwFile);
		
		PrintWriter out = response.getWriter();
		
		int i;
		while((i = iStream.read()) != -1) {
			out.write(i);
		}
		iStream.close();
		out.close();
		} catch (Exception ex) {
			throw new ControllerException("Can not download file.", ex);
		}
	}
	
	private void saveListOfferAsPDF(List<OfferLocalized> offers, HttpServletRequest request) {
		try {
			final String fileName = "Statistics.pdf";
			
			request.setAttribute(REQUEST_ATTR_PDF_FILE_NAME, fileName);
			
			final String path =  request.getRealPath("/");
			final String filePath = path + "\\" + fileName; 
			
			File file =  new File(fileName);
			if(file.exists()) {
				file.delete();
			}
			
			Document document = new Document();
			
			PdfWriter.getInstance(document, new FileOutputStream(filePath));
			
			document.open();

			//paragraph
			Paragraph paragraph = new Paragraph("Statistics of hall.");
			document.add(paragraph);
			
			document.add(new Paragraph(" "));
			
			//table
			PdfPTable table = new PdfPTable(9);
			
			PdfPCell tableCell = new PdfPCell(new Phrase("ID"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Hall"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Film"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Date"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Time"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Price"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Places"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Orders"));
			table.addCell(tableCell);
			
			tableCell = new PdfPCell(new Phrase("Occupancy"));
			table.addCell(tableCell);
			
			table.setHeaderRows(1);
			
			offers.sort(Comparator.comparing(OfferLocalized::getFilmId));
			
			for (OfferLocalized offer : offers) {
				String id = String.valueOf(offer.getOfferId());
				String hall = offer.getHallLocalizedName();
				String film = offer.getFilmLocalizedName();
				String date = offer.getOfferDate().toString();
				String time = offer.getOfferTime().toString();
				String price = offer.getOfferPrice().toString();
				String places = String.valueOf(offer.getHallSize());
				String orders = String.valueOf(offer.getOrderUnitsOrdered());
				String occupancy = String.valueOf(Math.round(10000.0 * offer.getOrderUnitsOrdered() / offer.getHallSize()) / 100.0);
				
				table.addCell(id);
				table.addCell(hall);
				table.addCell(film);
				table.addCell(date);
				table.addCell(time);
				table.addCell(price);
				table.addCell(places);
				table.addCell(orders);
				table.addCell(occupancy);
			}
			
			document.add(table);
			document.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
	
	public class HallStats{
		private final int offersNum;
		private final int placesNum;
		private final int ordersNum;
		private final double occupancy;
		
		public HallStats(final List<OfferLocalized> offers) {
			int places = 0;
			int orders = 0;
			
			for (OfferLocalized offer : offers) {
				places += offer.getHallSize();
				orders += offer.getOrderUnitsOrdered();
			}
			
			offersNum = offers.size();
			placesNum = places;
			ordersNum = orders;
			occupancy = Math.round(10000.0 * orders / places) / 100.0;
		}

		public int getOffersNum() {
			return offersNum;
		}

		public int getPlacesNum() {
			return placesNum;
		}

		public int getOrdersNum() {
			return ordersNum;
		}

		public double getOccupancy() {
			return occupancy;
		}
	}
}
