package classes.com.my.controllers.page.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.tools.navigation.PageRouter;

public class ProfilePageController extends BaseController {
	public static final String REQUEST_ATTR_INNER_PAGE = "profileInnerPage";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		super.goForward(request, response, PageRouter.PROFILE.getPath());
	}
	
	public static final void setInnerElement(HttpServletRequest request, String innerElem) throws ControllerException{
		request.setAttribute(REQUEST_ATTR_INNER_PAGE, innerElem);
	}
}
