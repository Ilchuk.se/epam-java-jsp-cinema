package classes.com.my.controllers.page.authorization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.util.EscapeTokenizer;

import classes.com.my.controllers.BaseController;
import classes.com.my.controllers.ControllerException;
import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.dbmanager.DBManagerException;
import classes.com.my.tools.dbmanager.viewmanagers.UserDataManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.UserData;
import classes.com.my.tools.hashing.MD5;
import classes.com.my.tools.navigation.PageRouter;

public class AuthorizarionPageController extends BaseController {	
	public static final String REQUEST_ATTR_VIEW_LOG_IN_MESSAGE = "logInMessage";
	public static final String REQUEST_ATTR_USER_LOGIN = "login";
	public static final String REQUEST_ATTR_USER_PASSWORD = "password";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ControllerException {			
		super.goForward(request, response, PageRouter.AUTHORISATION.getPath());
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ControllerException {
		try {
			String login = request.getParameter(REQUEST_ATTR_USER_LOGIN);
			String password = request.getParameter(REQUEST_ATTR_USER_PASSWORD);
			password = MD5.getHash(password);
						
			if(login == null){
				throw new IllegalArgumentException("Login field is empty");
			}else if (password == null) {
				throw new IllegalArgumentException("Password field is empty");
			}
			
			UserData currentUser = UserDataManager.getAuthorizedUser(login, password); 
			
			HttpSession session = request.getSession();
			if(currentUser != null) {
				session.setAttribute(GlobalAttributes.SESSION_ATTR_CUR_USER, currentUser);
				super.goForward(request, response, PageRouter.PROFILE.getPath());
				return;
			}else {
				throw new NullPointerException("No such user exist.");
			}
		}catch (DBManagerException ex) {
			// TODO: handle exception
			ex.printStackTrace();
			request.setAttribute(REQUEST_ATTR_VIEW_LOG_IN_MESSAGE, ex.getMessage());
		}
		catch (Exception ex) {
			request.setAttribute(REQUEST_ATTR_VIEW_LOG_IN_MESSAGE, ex.getMessage());
		}
		
		doGet(request, response);
	}
}
