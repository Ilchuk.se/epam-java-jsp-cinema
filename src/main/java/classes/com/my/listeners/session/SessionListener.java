package classes.com.my.listeners.session;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import classes.com.my.controllers.elements.ViewActionElement;
import classes.com.my.controllers.elements.footer.LangSelectAEContainer;
import classes.com.my.tools.attributes.GlobalAttributes;
import classes.com.my.tools.localizer.Localizer;
import classes.com.my.tools.localizer.LocalizerException;

public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		try {
			HttpSession session =  se.getSession();
			
			Locale locale = (Locale)session.getAttribute(GlobalAttributes.SESSION_ATTR_LOCALE);
			if(locale == null) {
				locale = new Locale(GlobalAttributes.LOCALE_DEFFAULT);
				session.setAttribute(GlobalAttributes.SESSION_ATTR_LOCALE, locale);
			}
			
			Localizer localizer = (Localizer) session.getAttribute(GlobalAttributes.SESSION_LOCALIZER);
			if(localizer == null) {
				
					localizer = new Localizer(GlobalAttributes.LOCALE_BUNDLE, locale);
				
				session.setAttribute(GlobalAttributes.SESSION_LOCALIZER, localizer);
			}
			
			List<ViewActionElement> langElems = (List<ViewActionElement>) session.getAttribute(GlobalAttributes.SESSION_ATTR_FOOTER_LANG_AE);
			if(langElems == null) {
				LangSelectAEContainer langContainer = new LangSelectAEContainer();
				langElems = langContainer.getAllElements(localizer);
				
				session.setAttribute(GlobalAttributes.SESSION_ATTR_FOOTER_LANG_AE, langElems);
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
	}
}
