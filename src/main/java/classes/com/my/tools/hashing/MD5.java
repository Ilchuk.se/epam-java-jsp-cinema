package classes.com.my.tools.hashing;

import java.security.MessageDigest;
import javax.xml.bind.DatatypeConverter;

public class MD5 {
	private static final String ALGOTITHM = "MD5";
	
	public static String getHash(String data){
		String hash = null;
		try {
			MessageDigest md = MessageDigest.getInstance(ALGOTITHM);
		    md.update(data.getBytes());
		    byte[] digest = md.digest();
		    
			hash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return hash;
	}
}
