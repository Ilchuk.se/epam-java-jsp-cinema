package classes.com.my.tools.attributes;

public class GlobalAttributes {
	public static final String REQUEST_ATTR_INNER_PAGE = "innerPage";
	public static final String REQUEST_ATTR_LANG = "lng";
	
	public static final String SESSION_ATTR_LOCALE = "locale";
	
	public static final String LOCALE_BUNDLE = "resources";
	public static final String LOCALE_DEFFAULT = "en";
	public static final String SESSION_LOCALIZER = "localizer";
	
	public static final String SESSION_ATTR_CUR_USER= "curSystemUser";
	
	public static final String SESSION_ATTR_FOOTER_LANG_AE = "footerLangAE";
	public static final String SESSION_ATTR_FOOTER_LANG_SELECTED = "footerLangSelected";
}
