package classes.com.my.tools.dbmanager.tablemanagers;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.DBManagerException;
import classes.com.my.tools.dbmanager.tablemanagers.entity.User;

public class UserManager {
	private static final String SQL_INSERT_NEW_USER = 
			"INSERT INTO cinema.user(login, password, nickname, system_role_id)" + 
			"VALUES(?, ?, ?, ?);";
	
	public static void addNewUser(User user) throws DBManagerException{
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			
			List<String> comParams = new ArrayList<>(4);
			comParams.add(user.getLogin());
			comParams.add(user.getPassword());
			comParams.add(user.getName());
			comParams.add(String.valueOf(user.getRoleId()));
			
			DBManager.executeSQLCommandWithoutResult(con, SQL_INSERT_NEW_USER, comParams);
			con.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			DBManager.rollbackConection(con);
			throw new DBManagerException("Please, try enter another login and try again.", ex);
		}
	}
}
