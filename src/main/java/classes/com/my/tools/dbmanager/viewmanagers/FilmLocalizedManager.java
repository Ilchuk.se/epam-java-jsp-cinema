package classes.com.my.tools.dbmanager.viewmanagers;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import classes.com.my.controllers.elements.ViewActionElement;
import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.FilmLocalized;

/**
 * Керує доступом до View БД під назвою cinema.film_localized.
 * 
 * @author Anatoliy Ilchuk
 */
public class FilmLocalizedManager {
	
	/**
	 * SQL-команда для пошуку усіх локалізованих фільмів.
	 */
	private static final String SQL_SELECT_ALL_FILM_LOCALIZED = 
			"SELECT * FROM cinema.film_localized FL " +
			"WHERE FL.language_name = ?";
	
	/**
	 * SQL-команда для пошуку усіх унікальних локалізованих фільмів.
	 */
	private static final String SQL_SELECT_ALL_UNIQUE_LOCALIZED_FILMS = 
			"SELECT F.film_id, F.localized_film_name FROM cinema.film_localized F"
			+ " WHERE F.language_name = ?"
			+ " GROUP BY F.film_id";
	
	/**
	 * Кількість стовпців у View БД під назвою cinema.film_localized.
	 */
	private static final int numColInTable = 9;
	
	/**
	 * Повертає список усіх локалізованих фільмів.
	 * 
	 * @param locale - локаль корисутвача.
	 * @return List<FilmLocalized> - список локалізованих фільмів.
	 */
	public static final List<FilmLocalized> getAllFilmLocalized(Locale locale) {
		List<FilmLocalized> flList = null;
		
		List<String> comParam = new ArrayList<>(1);
		comParam.add(locale.getLanguage());
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			
			List<List<String>> hlTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_ALL_FILM_LOCALIZED, comParam, numColInTable);
			flList = convertStringTableToFilmLocalizedList(hlTable);
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return flList;
	}
	
	/**
	 * Повертає локалізований список ViewActionElement для кожного фільму.
	 * 
	 * @param locale
	 * @return List<ViewActionElement> - список ViewActionElement для кожного фільму.
	 * @see ViewActionElement
	 */
	public static final List<ViewActionElement> getFilmFilterViewAEList(Locale locale){
		List<ViewActionElement> vaeList = null;
		
		List<String> comParam = new ArrayList<>(1);
		comParam.add(locale.getLanguage());
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			
			final int numColsToReturn = 2;
			
			List<List<String>> vaeTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_ALL_UNIQUE_LOCALIZED_FILMS, comParam, numColsToReturn);
			vaeList = convertStringTableToViewAEList(vaeTable);
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return vaeList;
	}
	
	/**
	 * Перетворює таблицю стрічок у об'єкт список локалізованих об'єктів класу ViewActionElement для фільму.
	 * @param table - таблиця, у якій збережено дані транзакції.
	 * @return List<ViewActionElement> -  список локалізованих об'єктів класу ViewActionElement для фільму.
	 */
	private static final List<ViewActionElement> convertStringTableToViewAEList(List<List<String>> table){
		List<ViewActionElement> vaeList = new ArrayList<>();
		
		for(int i = 0; i < table.get(0).size(); i++) {
			String value = getValueFromTable(table, 0, i);
			String label = getValueFromTable(table, 1, i);
			
			ViewActionElement viewAE = new ViewActionElement(value, label);
			
			vaeList.add(viewAE);
		}

		return vaeList;
	}
	
	/**
	 * Перетворює таблицю стрічок у список об'єктів FilmLocalized.
	 * 
	 * @see FilmLocalized
	 * @param table - таблиця, у якій збережено дані транзакції.
	 * @return List<FilmLocalized> -  список локалізованих об'єктів класу FilmLocalized.
	 */
	private static final List<FilmLocalized> convertStringTableToFilmLocalizedList(List<List<String>> table){
		List<FilmLocalized> flList = new ArrayList<>();
		
		for(int i = 0; i < table.get(0).size(); i++) {
			FilmLocalized film = new FilmLocalized();
			
			film.setId(Integer.parseInt(getValueFromTable(table, 0, i)));
			film.setName(getValueFromTable(table, 1, i));
			film.setLocalizedName(getValueFromTable(table, 7, i));
			
			flList.add(film);
		}
		
		return flList;
	}
	
	private static String getValueFromTable(List<List<String>> table, int col, int row) {
		String res = table.get(col).get(row);
		return res;
	}
}
