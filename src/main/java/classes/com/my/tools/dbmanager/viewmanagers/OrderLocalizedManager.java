package classes.com.my.tools.dbmanager.viewmanagers;

import java.sql.Connection;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.OrderLocalized;

/**
 * Керує доступом до View БД під назвою cinema.order_localized.
 * 
 * @author Anatoliy Ilchuk
 */
public class OrderLocalizedManager {
	/**
	 * SQL-команда для пошуку локалізованих квитків з вказаним корисутачем.
	 */
	private static final String SQL_SELECT_ALL_ORDERS_LOCALIZED_BY_USER_ID_STRING = 
			"SELECT * FROM cinema.order_localized OL"
			+ " WHERE OL.user_id = ?"
			+ " AND OL.language_name = ?";
	
	/**
	 * Кількість стовпців у View БД під назвою cinema.order_localized.
	 */
	private static final int numColInTable = 11;
	
	/**
	 * Повертає локалізований список квитків з вказаним корисутачем. 
	 * 
	 * @param locale - локаль користувача.
	 * @param id - ідентифікатор профілю користувача.
	 * @return List<OrderLocalized> - якщо користувач існує - список квитків користувача, якщо ні - пустий список.
	 */
	public static synchronized List<OrderLocalized> getAllOrdersLocalizedById(Locale locale, String id) {
		List<OrderLocalized> userOrders = new ArrayList<>();
		
		List<String> comParams = new ArrayList<>();
		comParams.add(id);
		comParams.add(locale.getLanguage());
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			
			List<List<String>> olTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_ALL_ORDERS_LOCALIZED_BY_USER_ID_STRING, comParams, numColInTable);
			userOrders = convertStringTableToOrderLocalizedList(olTable);
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return userOrders;
	}
	
	/**
	 * Перетворює таблицю стрічок у об'єкт список об'єктів класу OrderLocalized.
	 * 
	 * @param table - таблиця, у якій збережено дані транзакції.
	 * @return List<OrderLocalized> - список локалізованих замовлень.
	 */
	private static synchronized List<OrderLocalized> convertStringTableToOrderLocalizedList(List<List<String>> table) {
		List<OrderLocalized> olList = new ArrayList<>();
		
		for(int i = 0; i < table.get(0).size() ; i++) {
			OrderLocalized order = new OrderLocalized();
			
			order.setId(Integer.parseInt(getValueFromTable(table, 0, i)));
			order.setPlace(Integer.parseInt(getValueFromTable(table, 2, i)));
			order.setOfferId(Integer.parseInt(getValueFromTable(table, 3, i)));
			order.setDate(Date.valueOf(getValueFromTable(table, 4, i)));
			order.setTime(Time.valueOf(getValueFromTable(table, 5, i)));
			order.setHallName(getValueFromTable(table, 6, i));
			order.setFilmName(getValueFromTable(table, 7, i));
			order.setPrice(Float.parseFloat(getValueFromTable(table, 8, i)));
			
			olList.add(order);
		}
		
		return olList;
	}
	
	/**
	 * Забезпечує доступ до даних таблиці за індексом рядка та стовпця.
	 * 
	 * @param table - таблиця з якої потрібно отримати елемент.
	 * @param col - індекс стовпця таблиці.
	 * @param row - індекс рядка таблиці.
	 * @return String - дані збережені за вказаним індексом.
	 */
	private static String getValueFromTable(List<List<String>> table, int col, int row) {
		String res = table.get(col).get(row);
		return res;
	}
}
