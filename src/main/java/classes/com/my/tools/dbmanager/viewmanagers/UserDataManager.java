package classes.com.my.tools.dbmanager.viewmanagers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.DBManagerException;
import classes.com.my.tools.dbmanager.viewmanagers.entity.UserData;


/**
 * Керує доступом до View БД під назвою cinema.user_data.
 * 
 * @author Anatoliy Ilchuk
 *
 */
public class UserDataManager {
	/**
	 * SQL-команда для пошуку корстувача з вказаним та паролем.
	 */
	private static final String SQL_SELECT_CURRENT_USER = 
			"SELECT * FROM cinema.user_data UD "+
			"WHERE UD.login = ? "+ 
			"AND UD.password = ? ";
	
	/**
	 * Кількість стовпців у View БД під назвою cinema.user_data.
	 */
	private static final int numColInTable = 6;
	
	/**
	 * Повертає інформацію про існуючого користувача з заданим логіном та паролем.
	 * Якщо не існує, повертає  null.
	 * 
	 * @see UserData
	 * @param login - логін користувача.
	 * @param password - пароль користувача.
	 * @return UserData - дані профілю коритувача.
	 * @throws DBManagerException - виникає при складнощах доступу до БД, або некоректно сформованій транзакції.
	 */
	public static final UserData getAuthorizedUser(String login, String password) throws DBManagerException{
		UserData authUserData = null;
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			List<String> commandParams = new ArrayList<String>(2);
			commandParams.add(login);
			commandParams.add(password);
			List<List<String>> udTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_CURRENT_USER, commandParams, numColInTable);
			authUserData = convertStringTableToUserData(udTable);
		}catch (SQLException ex) {
			throw new DBManagerException("Can not get all OfferLoclized from DB view offer_localized", ex);
		}
		
		return authUserData;
	}
	
	/**
	 * Перетворює таблицю стрічок у об'єкт класу UserData.
	 * 
	 * @param table - таблиця, у якій збережено дані транзакції.
	 * @return UserData - інформація профілю користувача.
	 */
	private static UserData convertStringTableToUserData(final List<List<String>> table) {
		UserData uData = null;
		
		if(table != null && !table.isEmpty() && !table.get(0).isEmpty()) {
			uData = new UserData(); 
			uData.setUserId(Integer.parseInt(getValueFromTable(table, 0, 0)));
			uData.setUserNickname(getValueFromTable(table, 3, 0));
			uData.setSystemRoleId(Integer.parseInt(getValueFromTable(table, 4, 0)));
			uData.setSystemRoleName(getValueFromTable(table, 5, 0));
		}
		
		return uData;
	}
	
	/**
	 * Забезпечує доступ до даних таблиці за індексом рядка та стовпця.
	 * 
	 * @param table - таблиця з якої потрібно отримати елемент.
	 * @param col - індекс стовпця таблиці.
	 * @param row - індекс рядка таблиці.
	 * @return String - дані збережені за вказаним індексом.
	 */
	private static String getValueFromTable(List<List<String>> table, int col, int row) {
		String res = table.get(col).get(row);
		return res;
	}
}
