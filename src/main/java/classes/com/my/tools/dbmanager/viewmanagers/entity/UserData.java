package classes.com.my.tools.dbmanager.viewmanagers.entity;

/**
 * Дані профілю користувача, що відповідають View БД під назвою cinema.user_data.
 * 
 * @author Anatoliy Ilchuk
 */
public class UserData {
	/**
	 * Створює об'єкт даних профілю корисутвача
	 */
	public UserData() {
		super();
	}
	
	/**
	 * Ідентифікатор користувача
	 */
	private int userId;
	
	/**
	 * Ім'я користувача
	 */
	private String userNickname;
	
	/**
	 * Ідентифікатор ролі корисутвача в системі
	 */
	private int systemRoleId;
	
	/**
	 * Назва ролі корисутвача в системі
	 */
	private String systemRoleName;
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUserNickname() {
		return userNickname;
	}
	public void setUserNickname(String userNickname) {
		this.userNickname = userNickname;
	}
	public int getSystemRoleId() {
		return systemRoleId;
	}
	public void setSystemRoleId(int systemRoleId) {
		this.systemRoleId = systemRoleId;
	}
	public String getSystemRoleName() {
		return systemRoleName;
	}
	public void setSystemRoleName(String systemRoleName) {
		this.systemRoleName = systemRoleName;
	}
}
