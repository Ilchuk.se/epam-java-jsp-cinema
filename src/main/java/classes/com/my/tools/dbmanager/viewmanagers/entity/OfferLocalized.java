package classes.com.my.tools.dbmanager.viewmanagers.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

public class OfferLocalized {
	private int offerId;
	private Date offerDate;
	private Time offerTime;
	private BigDecimal offerPrice;

	private int languageId;
	private String languageName;

	private int filmId;
	private String filmOriginalName;
	private Time filmTimekeeping;
	private String filmPosterImgDir;
	private String filmLocalizedName;
	private String filmLocalizedDescription;

	private int hallId;
	private int hallSize;
	private String hallSchemeImgDir;
	private String hallLocalizedName;
	
	private int orderUnitsOrdered;
	
	public OfferLocalized() {
	}

	public int getOrderUnitsOrdered() {
		return orderUnitsOrdered;
	}

	public void setOrderUnitsOrdered(int orderUnitsOrdered) {
		this.orderUnitsOrdered = orderUnitsOrdered;
	}

	public static OfferLocalized getOffer() {
		return new OfferLocalized();
	}

	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public Date getOfferDate() {
		return offerDate;
	}

	public void setOfferDate(Date offerDate) {
		this.offerDate = offerDate;
	}

	public Time getOfferTime() {
		return offerTime;
	}

	public void setOfferTime(Time offerTime) {
		this.offerTime = offerTime;
	}

	public BigDecimal getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(BigDecimal offerPrice) {
		this.offerPrice = offerPrice;
	}

	public int getLanguageId() {
		return languageId;
	}

	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public int getFilmId() {
		return filmId;
	}

	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}

	public String getFilmOriginalName() {
		return filmOriginalName;
	}

	public void setFilmOriginalName(String filmOriginalName) {
		this.filmOriginalName = filmOriginalName;
	}

	public Time getFilmTimekeeping() {
		return filmTimekeeping;
	}

	public void setFilmTimekeeping(Time filmTimekeeping) {
		this.filmTimekeeping = filmTimekeeping;
	}

	public String getFilmPosterImgDir() {
		return filmPosterImgDir;
	}

	public void setFilmPosterImgDir(String fimPosterImgDir) {
		this.filmPosterImgDir = fimPosterImgDir;
	}

	public String getFilmLocalizedName() {
		return filmLocalizedName;
	}

	public void setFilmLocalizedName(String filmLocalizedName) {
		this.filmLocalizedName = filmLocalizedName;
	}

	public String getFilmLocalizedDescription() {
		return filmLocalizedDescription;
	}

	public void setFilmLocalizedDescription(String filmLocalizedDescription) {
		this.filmLocalizedDescription = filmLocalizedDescription;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public int getHallSize() {
		return hallSize;
	}

	public void setHallSize(int hallSize) {
		this.hallSize = hallSize;
	}

	public String getHallSchemeImgDir() {
		return hallSchemeImgDir;
	}

	public void setHallSchemeImgDir(String hallSchemeImgDir) {
		this.hallSchemeImgDir = hallSchemeImgDir;
	}

	public String getHallLocalizedName() {
		return hallLocalizedName;
	}

	public void setHallLocalizedName(String hallLocalizedName) {
		this.hallLocalizedName = hallLocalizedName;
	}
}
