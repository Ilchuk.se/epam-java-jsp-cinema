package classes.com.my.tools.dbmanager.viewmanagers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import classes.com.my.controllers.elements.mainpage.FilterOfferByDate;
import classes.com.my.controllers.elements.mainpage.SortOffer;
import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.DBManagerException;
import classes.com.my.tools.dbmanager.DBParser;
import classes.com.my.tools.dbmanager.viewmanagers.entity.OfferLocalized;

/**
 * Керує доступом до View БД під назвою cinema.offer_localized_with_orders.
 * 
 * @author Anatoliy Ilchuk
 */
public class OfferLocalizedManager {
	private OfferLocalizedManager() {
	}

	/**
	 * SQL-команда для пошуку локалізованих пропозицій продажу квитків від сьогодні до кінця місяцю.
	 */
	private static final String SQL_SELECT_OFFER_LOCALIZED = 
			"SELECT * FROM cinema.offer_localized_with_orders OL"
			+ " WHERE OL.language_name = ?";
	
	/**
	 * SQL-команда для пошуку локалізованої пропозиції продажу квитку від сьогодні до кінця місяцю з вказаним іднетифікатором.
	 */
	private static final String SQL_SELECT_OFFER_LOCALIZED_BY_ID = 
			"SELECT * FROM cinema.offer_localized_with_orders OL"
			+ " WHERE OL.language_name = ?"
			+ " AND OL.offer_id = ?";
	
	/**
	 * SQL-команда для пошуку локалізованих пропозицій продажу квитків з вказаними ідентифікаторами фільмів,
	 *  у часовому діапазоні від сьогодні і до потрібної дати, сортованих обраним за параметром на позиції від вказаної
	 *  і до вказаної.
	 */
	private static final String SQL_SELECT_PAGED_OFFERS_WITH_FILM_IDS = 
			"SELECT * FROM cinema.offer_localized_with_orders O"
			+ " WHERE"
			+ " O.language_name = ?"
			+ " AND O.date >= current_date()"
			+ " AND O.date <= current_date() + ?"
			+ " AND O.film_id IN (~)"
			+ " ORDER BY @"
			+ " LIMIT ?, ?";
	
	/**
	 * SQL-команда для пошуку кількості локалізованих пропозицій продажу квитків з вказаними ідентифікаторами фільмів,
	 *  у часовому діапазоні від сьогодні і до потрібної дати.
	 */
	private static final String SQL_SELECT_NUMBER_OF_PAGED_OFFERS_WITH_FILM_IDS = 
			"SELECT count(offer_id) FROM cinema.offer_localized_with_orders O"
			+ " WHERE"
			+ " O.language_name = ?"
			+ " AND O.date >= current_date()"
			+ " AND O.date <= current_date() + ?"
			+ " AND O.film_id IN (~)";
	
	/**
	 * SQL-команда для пошуку локалізованих пропозицій продажу квитків 
	 *  у часовому діапазоні від сьогодні і до потрібної дати, сортованих обраним за параметром на позиції від вказаної
	 *  і до вказаної.
	 */
	private static final String SQL_SELECT_PAGED_OFFERS = 
			"SELECT * FROM cinema.offer_localized_with_orders O"
			+ " WHERE"
			+ " O.language_name = ?"
			+ " AND O.date >= current_date()"
			+ " AND O.date <= current_date() + ?"
			+ " ORDER BY @"
			+ " LIMIT ?, ?";
	
	/**
	 * SQL-команда для пошуку кількості локалізованих пропозицій продажу квитків
	 *  у часовому діапазоні від сьогодні і до потрібної дати.
	 */
	private static final String SQL_SELECT_NUMBER_OF_PAGED_OFFERS = 
			"SELECT count(offer_id) FROM cinema.offer_localized_with_orders O"
			+ " WHERE"
			+ " O.language_name = ?"
			+ " AND O.date >= current_date()"
			+ " AND O.date <= current_date() + ?";
	
	/**
	 * Кількість стовпців у View БД під назвою cinema.offer_localized_with_orders.
	 */
	private static final int numColInTable = 17;
	
	/**
	 * Повертає список усіх локалізованих пропозицій продажу квитків.
	 * 
	 * @param locale - локаль корисутвача.
	 * @return List<OfferLocalized> - список локалізованих пропозицій продажу квитків.
	 * @throws DBManagerException - виникає коли є помилки в винтаксисі команди.
	 */
	public static List<OfferLocalized> getAllOffersLocalized(Locale locale) throws DBManagerException{
		List<OfferLocalized> olList = null;
		
		List<String> comParam = new ArrayList<>();
		comParam.add(locale.getLanguage());
		
		try {
			Connection con = DBManager.getInstance().getConnection();

			List<List<String>> olTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_OFFER_LOCALIZED, comParam, numColInTable);
			olList = convertStringTableToOfferLocalizedList(olTable);
		}catch (SQLException ex) {
			throw new DBManagerException("Can not get all OfferLoclized from DB view offer_localized", ex);
		}
		
		return olList;
	}
	
	/**
	 * Повертає список локалізовану пропозицію продажу квитку з обраним ідентифікатором.
	 * @param locale - локаль корисутвача.
	 * @param id - ідентифікатор пропозиції.
	 * @return OfferLocalized - пропозиція квитку.
	 */
	public static OfferLocalized getOfferLocalizedById(Locale locale, String id) {
		OfferLocalized resOffer = null;
		
		List<String> comParam = new ArrayList<>();
		comParam.add(locale.getLanguage());
		comParam.add(id);
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			
			List<List<String>> olTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_OFFER_LOCALIZED_BY_ID, comParam, numColInTable);
			List<OfferLocalized> olList = convertStringTableToOfferLocalizedList(olTable);
			if(olList != null && !olList.isEmpty()) {
				resOffer = olList.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return resOffer;
	}
	
	/**
	 * Повертає кількість пропозицій, що відповідають елементам фільтрації.
	 * 
	 * @param locale - локаль користувача
	 * @param filter - фільтр за датою перегляду.
	 * @param sort - тип сортування.
	 * @param selectedFilms - список обраних фільмів.
	 * @return int - квількість знайдених пропозицій.
	 */
	public static final int getNumberOfFilteredOfferLocalized(Locale locale, FilterOfferByDate filter, SortOffer sort, List<Integer> selectedFilms) {		
		List<String> comParam = new ArrayList<>();
		comParam.add(locale.getLanguage());
		comParam.add(filter.getDays());
					
		try {
			Connection con = DBManager.getInstance().getConnection();
			
			List<List<String>> olTable;
			
			final int numColsToReturn = 1;
			
			if(selectedFilms != null && !selectedFilms.isEmpty()) {
				final String SQL_COMMAND = DBParser.replaceTildaWithIntList(SQL_SELECT_NUMBER_OF_PAGED_OFFERS_WITH_FILM_IDS, selectedFilms);
				olTable = DBManager.executeSQLCommandWithResult(con, SQL_COMMAND, comParam, numColsToReturn);
			}else {
				olTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_NUMBER_OF_PAGED_OFFERS, comParam, numColsToReturn);
			}
			
			return Integer.parseInt(olTable.get(0).get(0));
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return 0;
	}
	
	/**
	 * Повертає писок пропозицій, що відповідають елементам фільтрації.
	 * 
	 * @param locale - локаль користувача
	 * @param filter - фільтр за датою перегляду.
	 * @param sort - тип сортування.
	 * @param selectedFilms - список обраних фільмів.
	 * @param firstIndex - початковий порядковий номер знайдених фільмів.
	 * @param lastIndex - кінцевий порядковий номер знайдених фільмів.
	 * @return List<OfferLocalized> - список фільмів.
	 */
	public static final List<OfferLocalized> getAllFilteredOfferLocalized(Locale locale, FilterOfferByDate filter, SortOffer sort, List<Integer> selectedFilms, int firstIndex, int lastIndex) {
		List<OfferLocalized> olList = null;
		
		List<String> comParam = new ArrayList<>();
		comParam.add(locale.getLanguage());
		comParam.add(filter.getDays());
		comParam.add(String.valueOf(firstIndex));
		comParam.add(String.valueOf(lastIndex));
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			
			List<List<String>> olTable;
			
			if(selectedFilms != null && !selectedFilms.isEmpty()) {
				String SQL_COMMAND = DBParser.replaceTildaWithIntList(SQL_SELECT_PAGED_OFFERS_WITH_FILM_IDS, selectedFilms);
				SQL_COMMAND = DBParser.replaceAllDogsInStringWithValue(SQL_COMMAND, sort.getSQLCommand()); 
				olTable = DBManager.executeSQLCommandWithResult(con, SQL_COMMAND, comParam, numColInTable);
			}else {
				String SQL_COMMAND = DBParser.replaceAllDogsInStringWithValue(SQL_SELECT_PAGED_OFFERS, sort.getSQLCommand());
				olTable = DBManager.executeSQLCommandWithResult(con, SQL_COMMAND, comParam, numColInTable);
			}
			
			olList = convertStringTableToOfferLocalizedList(olTable);
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return olList;
	}
	
	/**
	 * Перетворює таблицю стрічок у об'єкт список об'єктів класу OfferLocalized.
	 * @param offerLocTable - таблиця, у якій збережено дані транзакції.
	 * @return List<OfferLocalized> -  список локалізованих пропозицій.
	 */
	private static List<OfferLocalized> convertStringTableToOfferLocalizedList(final List<List<String>> offerLocTable) {
		List<OfferLocalized> offersFromTable = new ArrayList<>();
		
		for(int i = 0; i < offerLocTable.get(0).size(); i++) {
			OfferLocalized offer = OfferLocalized.getOffer();
			
			int colNum = 0;
			
			offer.setOfferId(Integer.parseInt(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setOfferDate(Date.valueOf(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setOfferTime(Time.valueOf(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setOfferPrice(new BigDecimal(getValueFromTable(offerLocTable, colNum++, i)));
			
			offer.setLanguageId(Integer.parseInt(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setLanguageName(getValueFromTable(offerLocTable, colNum++, i));
			
			offer.setFilmId(Integer.parseInt(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setFilmOriginalName(getValueFromTable(offerLocTable, colNum++, i));
			offer.setFilmTimekeeping(Time.valueOf(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setFilmPosterImgDir(getValueFromTable(offerLocTable, colNum++, i));
			offer.setFilmLocalizedName(getValueFromTable(offerLocTable, colNum++, i));
			offer.setFilmLocalizedDescription(getValueFromTable(offerLocTable, colNum++, i));
			
			offer.setHallId(Integer.parseInt(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setHallSize(Integer.parseInt(getValueFromTable(offerLocTable, colNum++, i)));
			offer.setHallSchemeImgDir(getValueFromTable(offerLocTable, colNum++, i));
			offer.setHallLocalizedName(getValueFromTable(offerLocTable, colNum++, i));
			
			offer.setOrderUnitsOrdered(Integer.parseInt(getValueFromTable(offerLocTable, colNum++, i)));
			
			offersFromTable.add(offer);
		}
		
		return offersFromTable;
	}
	
	/**
	 * Забезпечує доступ до даних таблиці за індексом рядка та стовпця.
	 * 
	 * @param table - таблиця з якої потрібно отримати елемент.
	 * @param col - індекс стовпця таблиці.
	 * @param row - індекс рядка таблиці.
	 * @return String - дані збережені за вказаним індексом.
	 */
	private static String getValueFromTable(List<List<String>> table, int col, int row) {
		String res = table.get(col).get(row);
		return res;
	}
}
