package classes.com.my.tools.dbmanager.tablemanagers;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.DBManagerException;
import classes.com.my.tools.dbmanager.tablemanagers.entity.Offer;


public class OfferManager {
	private static final String SQL_INSERT_NEW_OFFER = 
			"INSERT INTO cinema.offer(film_id, cinema_hall_id, date, time, price) " + 
			"VALUES(?, ?, ?, ?, ?)";
	
	public static final void insertNewOffer(Offer offer) throws DBManagerException {
		List<String> comParam = new ArrayList<>();
		comParam.add(String.valueOf(offer.getFilmId()));
		comParam.add(String.valueOf(offer.getHallId()));
		comParam.add(String.valueOf(offer.getDate()));
		comParam.add(String.valueOf(offer.getTime()));
		comParam.add(String.valueOf(offer.getPrice()));
		
		Connection con = null;
		
		try {
			con = DBManager.getInstance().getConnection();
			
			DBManager.executeSQLCommandWithoutResult(con, SQL_INSERT_NEW_OFFER, comParam);
			con.commit();
		} catch (Exception ex) {
			DBManager.rollbackConection(con);
			ex.printStackTrace();
			throw new DBManagerException("Can not insert new user with params: " + comParam.toString() , ex);
		}
	}
}
