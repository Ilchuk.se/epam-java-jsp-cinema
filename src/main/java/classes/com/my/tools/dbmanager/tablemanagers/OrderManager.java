package classes.com.my.tools.dbmanager.tablemanagers;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.DBManagerException;
import classes.com.my.tools.dbmanager.tablemanagers.entity.Order;


public class OrderManager {
	private static final String SQL_GET_BOOCKED_PLACES_BY_OFFER_ID_STRING = 
			"SELECT O.place FROM cinema.order O"
			+ " WHERE O.ticket_id = ?;";
	
	private static final String SQL_INSERT_ORDER = 
			"INSERT INTO cinema.order(ticket_id, user_id, place, is_paid) " + 
			"VALUES( ?, ?, ?, true);";
	
	public static List<Integer> getAllBookedPlacesByOfferID(String offerID){
		List<Integer> bookedPlaces = new ArrayList<>();
		int numColInTable = 1;
		
		List<String> comParams = new ArrayList<>(1);
		comParams.add(offerID);
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			List<List<String>> bpTable = DBManager.executeSQLCommandWithResult(con, SQL_GET_BOOCKED_PLACES_BY_OFFER_ID_STRING, comParams, numColInTable);
			if(bpTable != null && !bpTable.isEmpty()) {
				bookedPlaces = bpTable.get(0).stream().map(Integer::parseInt).collect(Collectors.toList());
			}
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return bookedPlaces;
	}
	
	public static void addNewOrders(List<Order> orders) throws DBManagerException{
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			
			
			for (Order order : orders) {
				List<String> comParams = new ArrayList<>(3);
				
				comParams.add(String.valueOf(order.getTicketId()));
				comParams.add(String.valueOf(order.getUserId()));
				comParams.add(String.valueOf(order.getPlace()));
				
				DBManager.executeSQLCommandWithoutResult(con, SQL_INSERT_ORDER, comParams);
			}
			
			con.commit();
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
			DBManager.rollbackConection(con);
			throw new DBManagerException("Can not add users.", ex);
		}
	}
}
