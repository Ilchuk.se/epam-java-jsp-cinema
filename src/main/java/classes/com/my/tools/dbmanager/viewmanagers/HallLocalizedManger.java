package classes.com.my.tools.dbmanager.viewmanagers;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import classes.com.my.tools.dbmanager.DBManager;
import classes.com.my.tools.dbmanager.viewmanagers.entity.HallLocalized;

/**
 * Керує доступом до View БД під назвою cinema.cinema_hall_localized.
 * 
 * @author Anatoliy Ilchuk
 */
public class HallLocalizedManger {
	
	/**
	 * SQL-команда для пошуку усіх локалізованих залів кінотеатру.
	 */
	private static final String SQL_SELECT_ALL_HALL_LOCALIZED = 
			"SELECT * FROM cinema.cinema_hall_localized HL " +
			"WHERE HL.language_name = ?";
	
	/**
	 * Кількість стовпців у View БД під назвою cinema.cinema_hall_localized.
	 */
	private static final int numColInTable = 6;
	
	/**
	 * Повертає список усіх локалізованих залів кінотеатру.
	 * @param locale - локаль корисутвача.
	 * @return List<HallLocalized> - список локалізованих залів кінотеатру.
	 */
	public static final List<HallLocalized> ListgetAllHallLocalized(Locale locale) {
		List<HallLocalized> hlList = null;
		
		List<String> comParam = new ArrayList<>(1);
		comParam.add(locale.getLanguage());
		
		try {
			Connection con = DBManager.getInstance().getConnection();
			
			List<List<String>> hlTable = DBManager.executeSQLCommandWithResult(con, SQL_SELECT_ALL_HALL_LOCALIZED, comParam, numColInTable);
			hlList = convertStringTableToHallLocalizedList(hlTable);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return hlList;
	}
	
	/**
	 * Перетворює таблицю стрічок у список об'єктів класу HallLocalized.
	 * @param table - таблиця, у якій збережено дані транзакції.
	 * @return List<HallLocalized> -  список локалізованих залів кінотеатру.
	 */
	private static final List<HallLocalized> convertStringTableToHallLocalizedList(List<List<String>> table) {
		List<HallLocalized> hlList = new ArrayList<>();
		
		for(int i = 0; i < table.get(0).size(); i++) {
			HallLocalized hall = new HallLocalized();
			
			hall.setId(Integer.parseInt(getValueFromTable(table, 0, i)));
			hall.setSize(Integer.parseInt(getValueFromTable(table, 1, i)));
			hall.setName(getValueFromTable(table, 5, i));
			
			hlList.add(hall);
		}
		
		return hlList;
	}
	
	/**
	 * Забезпечує доступ до даних таблиці за індексом рядка та стовпця.
	 * 
	 * @param table - таблиця з якої потрібно отримати елемент.
	 * @param col - індекс стовпця таблиці.
	 * @param row - індекс рядка таблиці.
	 * @return String - дані збережені за вказаним індексом.
	 */
	private static String getValueFromTable(List<List<String>> table, int col, int row) {
		String res = table.get(col).get(row);
		return res;
	}
}
