package classes.com.my.tools.dbmanager;

/**
 * Помилка, що виникає у результаті життєвого циклу об'єкту DBManager або його нащадків.
 * 
 * @author Anatoliy Ilchuk
 */
public class DBManagerException extends Exception{
	private static final long serialVersionUID = 1L;

	/**
	 * Створює exception з полями message(можна отримати через функцію getMessage()) та cause.
	 * 
	 * @param message - String, причина виникнення помилки.
	 * @param cause - Throwable, exception який виникнув під час робити класу DBManager або його нащадків.
	 */
	public DBManagerException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
