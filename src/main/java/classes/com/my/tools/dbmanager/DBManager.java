package classes.com.my.tools.dbmanager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.taglibs.standard.tag.common.xml.IfTag;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.Context;

/**
 * Контролює підключення та звернення до БД.
 * 
 * @author Anatoliy Ilchuk
 */
public class DBManager {
	/**
	 * Singleton, даного класу.
	 */
	private static DBManager instance;
	
	/**
	 * Метод повертає Singleton об'єкт даного класу.
	 * 
	 * @return DBManager - instance даного класу.
	 */
	public static synchronized DBManager getInstance() {
		if(instance == null) {
			instance = new DBManager();
		}
		return instance;
	}
	
	/**
	 * Створює об'єкт даного класу конфігурууючи ConnectionPool на сервері ApacheTomcat.
	 * Детальніші параметри пулу можна знайти у файлі webapp/META-INF/context.xml
	 */
	private DBManager() {
		try {
		Context initContext = new InitialContext();
		Context envContext  = (Context)initContext.lookup("java:/comp/env");
		ds = (DataSource)envContext.lookup("jdbc/TestDB");
		}catch (NamingException ex) {
			throw new IllegalStateException("Can not init DBMamager", ex);
		}
	}
	
	//////////////
	
	/**
	 * Точка входу у ConnectionPool проекту.
	 */
	private static DataSource ds;
	
	/**
	 * Повертає Connection отриманий з ConnectionPool на сервері ApacheTomcat.
	 * 
	 * @return Connection - з'єднання для роботи з БД.
	 * @throws SQLException - якщо виникли склднощі у роботі з БД.
	 */
	public synchronized Connection getConnection() throws SQLException{
		return ds.getConnection();
	}
	
	/////////////
	
	/**
	 * На основі з'єднання з БД, SQL-команди, її параметрів, та кількості стовпців індексів, які будуть створені
	 *  виконує команду на сервері та повертає результат у вигляді таблиці(матриці) стрічок(List<List<String>>).
	 * 
	 * Рекомендується використувати для INSERT команд, коли необхідно повернути згенеровані індекси.
	 * 
	 * Реалізує фасад для методу executeSQLCommand.
	 * 
	 * @see executeSQLCommand
	 * @param con - з'єднання з БД.
	 * @param SQL_Command - команада мовою SQL.
	 * @param comParams - список параметрів, які необхідно динамічно підставити у команду на місце символу'?'.
	 * @param generatedIndexesCount - кілкість стовпчиків у вихідній таблиці.
	 * @return List<List<String>> - результуюча таблиця(матриця) створених індексів.
	 * @throws DBManagerException - виникає при помилці звернення або виконання команди на БД.
	 */
    public static List<List<String>> executeSQLCommandWithKeyGeneration(Connection con, String SQL_Command, List<String> comParams, int generatedIndexesCount) throws DBManagerException{
        return executeSQLCommand(con, SQL_Command, comParams, generatedIndexesCount, true);
    }

    /**
     * На основі з'єднання з БД, SQL-команди, її параметрів, та кількості стовпців, що повернуться у результаті виконання команди
	 *  виконує команду на сервері та повертає результат у вигляді таблиці(матриці) стрічок(List<List<String>>).
	 *  
	 *  Рекомендується використувати для SELECT команд, коли необхідно повернути дані з БД.
	 *  
	 *  Реалізує фасад для методу executeSQLCommand.
	 * 
	 * @see executeSQLCommand
     * @param con - з'єднання з БД.
     * @param SQL_Command - команада мовою SQL
     * @param comParams - список параметрів, які необхідно динамічно підставити у команду на місце символу'?'.
     * @param numColToReturn - кілкість стовпців, яку буде повернено після виконання команди.
     * @return List<List<String>> - результуюча таблиця(матриця) результатів виконання команди.
     * @throws DBManagerException - виникає при помилці звернення або виконання команди на БД.
     */
    public static List<List<String>> executeSQLCommandWithResult(Connection con, String SQL_Command, List<String> comParams, int numColToReturn) throws DBManagerException{
        return executeSQLCommand(con, SQL_Command, comParams, numColToReturn, false);
    }

    /**
     * На основі з'єднання з БД, SQL-команди та її параметрів
	 *  виконує команду на сервері без генерації результату.
	 *  
	 *  Рекомендується використувати для DELETE, UPDATE команд, коли не потрібно повертати дані з БД.
     * 
     * Реалізує фасад для методу executeSQLCommand.
	 * 
	 * @see executeSQLCommand
     * @param con - з'єднання з БД.
     * @param SQL_Command - команада мовою SQL
     * @param comParams - список параметрів, які необхідно динамічно підставити у команду на місце символу'?'.
     * @throws DBManagerException - виникає при помилці звернення або виконання команди на БД.
     */
    public static void executeSQLCommandWithoutResult(Connection con, String SQL_Command, List<String> comParams) throws DBManagerException{
        executeSQLCommand(con, SQL_Command, comParams, 0, false);
    }

    /**
     * Універсальний метод для звернення будь-якого типу до БД.
     * Підходить для усіх CRUD операцій. 
     *  
     * @param con  - з'єднання з БД.
     * @param command - команада мовою SQL.
     * @param comParams - список параметрів, які необхідно динамічно підставити у команду на місце символу'?'.
     * @param numColToReturn - кількість стовпців резулбтуючої таблиці.
     * @param shouldReturnGeneratedKeys - метод повинен поертати: згенеровані ключі - true, результат виконання команди - false. 
     * @return List<List<String>> - результуюча таблиця(матриця).
     * @throws DBManagerException - виникає при помилці звернення або виконання команди на БД.
     */
    private static List<List<String>> executeSQLCommand(Connection con, String command, List<String> comParams, int numColToReturn, boolean shouldReturnGeneratedKeys) throws DBManagerException{
        PreparedStatement pst = null;
        ResultSet rs = null;
        List<List<String>> transactionResult = null;

        try{
            pst = createPreparedStatement(con, command, shouldReturnGeneratedKeys);
            setParamsToPreparedStatement(pst, comParams);
            pst.execute();

            if (numColToReturn < 1) {
                return null;
            }

            transactionResult = new ArrayList<>();
            initializeTableBySize(transactionResult,numColToReturn);
            rs = getResultSetFromStatement(pst, shouldReturnGeneratedKeys);

            while (rs.next()) {
                addRowToTableFromResultSet(rs, transactionResult, numColToReturn, shouldReturnGeneratedKeys);
            }
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
            throw new DBManagerException("Can not execute sql command: " + command, ex);
        }finally {
            close(pst);
            close(rs);
        }

        return transactionResult;
    }
    
    /**
     * Створює PreparedStatement на основі з'єднання, команди та списку параметрів.
     * 
     * @param con - з'єднання з БД.
     * @param SQL_Command - команада мовою SQL.
     * @param shouldReturnGeneratedKeys - метод повинен поертати: згенеровані ключі - true, результат виконання команди - false. 
     * @return PreparedStatement - динамічно створене вираження для виконання на БД.
     * @throws SQLException - винкає, коли з'єднання закрите.
     */
    private static PreparedStatement createPreparedStatement(Connection con,String SQL_Command,boolean shouldReturnGeneratedKeys) throws SQLException{
        PreparedStatement pst;
        if(shouldReturnGeneratedKeys){
            pst = con.prepareStatement(SQL_Command, Statement.RETURN_GENERATED_KEYS);
        }else {
            pst = con.prepareStatement(SQL_Command);
        }

        return pst;
    }

    /**
     *  Встановлює прараметри в PreparedStatement замість символу '?', у порядку заданому у списку. 
     *  
     * @param pst - PreparedStatement, у який буде підставлено прарамметри.
     * @param params - список параметрів SQL-команди.
     * @throws SQLException - виникає, якщо індекс параметру зі списку, більший за кількість всіх параметрів у команді,
     * або PreparedStatement є закритим.
     */
    private static void setParamsToPreparedStatement(PreparedStatement pst, List<String> params) throws SQLException{
        if (params != null && !params.isEmpty()) {
            for (int i = 0; i < params.size(); i++) {
            	
            	String param = params.get(i);
            	
            	if(DBParser.tryParseInt(param)) {
            		pst.setInt(i + 1, Integer.valueOf(param));
            	}else {
            		pst.setString(i + 1, param);
            	}
            }
        }
    }

    /**
     * Повертає ResultSet зі згенерованими індексами або з резултатом виконання команди.
     * В залежнотсі від параметру shouldReturnGeneratedKeys.
     * 
     * @param pst - PreparedStatement, з якого отримується резельтат виконання комманди.
     * @param shouldReturnGeneratedKeys - метод повинен повертати: згенеровані ключі - true, результат виконання команди - false. 
     * @return ResultSet - результуюча множина викононої команди.
     * @throws SQLException - якщо виникли склданощі з доступрм до БД, або PreparedStatement є закритим.
     */
    private static ResultSet getResultSetFromStatement(PreparedStatement pst, boolean shouldReturnGeneratedKeys) throws SQLException{
        ResultSet rs;
        if(shouldReturnGeneratedKeys){
            rs = pst.getGeneratedKeys();
        }else {
            rs = pst.getResultSet();
        }

        return rs;
    }

    /**
     * Створює таблицю(List<List<String>> Table) з заданою кількість стовпчиків.
     * 
     * @param Table - таблиця, яку потрібно сформувати з з заданою кількість стовпчиків.
     * @param size - кількість стовпчиків у таблиці.
     */
    private static void initializeTableBySize(List<List<String>> Table, int size){
        for (int i = 0; i < size; i++) {
            Table.add(new ArrayList<>());
        }
    }

    /**
     * Витягуює дані з ResultSet та додає їх у таблицю.
     * 
     * @param rs - ResultSet, виконаної команди.
     * @param table- таблиця в яку потрібно записати результат виконання команди.
     * @param numColToReturn - кідбкість стовпчиків у ResultSet та таблиці.
     * @param shouldReturnGeneratedKeys - метод повинен повертати: згенеровані ключі - true, результат виконання команди - false. 
     * @throws SQLException - виникає коли доступаєшся до неіснюючого індеку; є склданощі з БД або ResultSet закритий.
     */
    private static void addRowToTableFromResultSet(ResultSet rs, List<List<String>> table, int numColToReturn, boolean shouldReturnGeneratedKeys) throws SQLException{
        for (int i = 0; i < numColToReturn; i++) {
        	table.get(i).add(rs.getString(i + 1));
        }
    }

    /**
     * Метод закриває PreparedStatement.
     * 
     * @param ps - PreparedStatement, який потрібно закрити.
     */
    private static void close(PreparedStatement ps){
        if(ps != null){
            try {
                ps.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    /**
     * Метод закриває ResultSet.
     * 
     * @param rs - ResultSet, який потрібно закрити.
     */
    private static void close(ResultSet rs){
        if(rs != null){
            try {
                rs.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Виконує rollback транзакції.
     * 
     * @param connection - з'єднання на якому потрбно виконати rollback. 
     */
    public static final void rollbackConection(Connection connection) {
    	try {
    		if(connection != null) {
    			connection.rollback();
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }
}
