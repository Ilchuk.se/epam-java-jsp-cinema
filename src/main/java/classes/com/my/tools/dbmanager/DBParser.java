package classes.com.my.tools.dbmanager;

import java.util.List;

/**
 * Вмконує операції з преретворення та модифікацій стрічок
 * 
 * @author Anatoliy Ilchuk
 *
 */
public class DBParser {
	/**
	 * Перевіряє чи можливо привести стрічку до типу int.
	 * 
	 * @param value - String, стрічкове значення яке потрібно перевірити на можливість привести до типу int.
	 * @return boolean - Якщо можна привести до типу int - true, якщо ні - false.
	 */
	public static boolean tryParseInt(String value) {  
	     try {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch (NumberFormatException e) {  
	         return false;  
	      }  
	}
	
	
	/**
	 * Знаходить усі входження символу '~' у стрічку та
	 * замінює їх списком Integer у вигляді 'n,n, ... n'
	 * 
	 * @param inputString - String, вхідна стрічка, яку потрібно модифікувати.
	 * @param intList - List<Integer>, список який потрібно підставити у стрічку замість символу '~'.
	 * @return String - модифікована стрічка.
	 */
	public static String replaceTildaWithIntList(final String inputString, List<Integer> intList) {
		StringBuilder sBuilder = new StringBuilder();
		intList.forEach(x -> sBuilder.append(x).append(","));
		sBuilder.setLength(sBuilder.length() - 1);
		
		String result = inputString.replace("~", sBuilder.toString());
		return result;
	}
	
	public static String replaceAllDogsInStringWithValue(final String inputString, String replacement) {
		String result = inputString.replace("@", replacement);
		return result;
	} 
}
