package classes.com.my.tools.localizer;

import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Клас створений для локалцізації інтрефейсу корстувача.
 * Отримавши адресу бандлу та мову, клас повертає запитувані значення.
 * 
 * @author Anatoliy Ilchuk
 */
public class Localizer implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Назва бандлу з якого будуть зчитуватись дані.
	 */
	private ResourceBundle bundle;
	
	/**
	 * Мова локалізаціх для бандлу.
	 */
	private String lang;
	
	/**
	 * 
	 * @param bundleName - String, назва запитуваного бандлу.
	 * @param loc - locale, локаль для обраного бандлу.
	 * @throws LocalizerException - якщо не може знайти запитуваний бандл, або його назва рівна null.
	 */
	public Localizer(String bundleName, Locale loc) throws LocalizerException{
		try {
			bundle =  ResourceBundle.getBundle(bundleName, loc);
			lang = loc.getLanguage();
		} catch (Exception ex) {
			throw new LocalizerException("Can not find bundle - " + bundleName + " with locale " + loc, ex);
		}
		
	}
	
	/**
	 * Метод повертає локалізований запис з бандлу за його назвою(тегом). 
	 * 
	 * @param propName - String, назва запису(тег).
	 * @return text - String, текст отриманий за вказаним тегом.
	 * Якщо даного запису не існує, то повертає null.
	 */
	public synchronized String getValue(String propName){
		String value = null;
		try {
			value = bundle.getString(propName);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return value;
	};
	
	/**
	 * Повертає обрану при створенні об'єкту мову.
	 * 
	 * @return language - String, обрана при створенні об'єкту мова.
	 */
	public synchronized String getLang() {
		return lang;
	}
}
