package classes.com.my.tools.localizer;

/**
 * Дана помилка виникає у разі складнощів оботи класу Localizer. 
 * 
 * @author Anatoliy Ilchuk
 */
public class LocalizerException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Сторює exception з полями message(можна отримати через функцію getMessage()) та cause.
	 * 
	 * @param message - String, причина виникнення помилки.
	 * @param cause - Throwable, exception який виникнув під час робити класу Localizer.
	 */
	public LocalizerException(String message, Throwable cause) {
		super(message, cause);
	}
}
