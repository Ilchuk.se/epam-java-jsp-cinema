package classes.com.my.tools.navigation;

import javax.servlet.http.HttpServletRequest;

/**
 * Цей Enum створений для полегшення навігації між елементами.
 * З кожного елементу можна отримати внутрішній шлях до JSP-файлу та його URL-значення.
 *
 * @author Anatolii Ilchuk
*/
public enum PageRouter {
	HEADER_DEFFAULT("../elements/Header.jsp", "null"), 
	HEADER_ATHORIZED("../elements/AuthorizedUserHeader.jsp", "null"),
	FOOTER("../elements/Footer.jsp", "/lang"),
	
	TEMPLATE("/jsp/pages/PageTemplate.jsp", "null"),
	ERROR("/jsp/pages/error/ErrorPage.jsp", "null"),
	
	MAIN("/jsp/pages/main/MainPage.jsp", "/"),
	FILM_DESCRIPTION("/jsp/pages/filmdescription/FilmOfferPage.jsp", "/film"),
	
	REGISTRATION("/jsp/pages/registration/RegistrationPage.jsp", "/registration"),
	AUTHORISATION("/jsp/pages/authorization/AuthorizationPage.jsp", "/authorization"),
	
	PROFILE("/jsp/pages/profile/ProfilePage.jsp", "/profile"),
	PROFILE_INFO("/jsp/pages/profile/elements/ProfileInfoPage.jsp", "/profile/info"),
	PROFILE_OFFER("/jsp/pages/profile/elements/ProfileOfferPage.jsp", "/profile/offer"),
	PROFILE_OFFER_NEW("/jsp/pages/profile/elements/ProfileAddOfferPage.jsp", "/profile/offer/new"),
	PROFILE_STATISTICS("/jsp/pages/profile/elements/ProfileStatisticsPage.jsp", "/profile/statistics"),
	
	ORDERS("/jsp/pages/cart/OrdersPage.jsp", "/orders")
	;
	
	private PageRouter(String path, String URL) {
		this.path = path;
		this.URL = URL;
	}
	
	/**
	 * Поле містить внутрішню адресу JSP-файлу.
	 */
	private final String path;
	
	/**
	 * Поле містить URL-адресу.
	 */
	private final String URL;

	/**
	 * Метод повертає адресу JSP-файлу.
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * Метод повертає URL-адресу сторінки.
	 */
	public String getURL() {
		return URL;
	}
	
	/**
	 * Функція формує повну URL-адресу з урахуванням ROOT серверу.
	 * 
	 * @param request - HttpServletRequest, отриманий з контроллеру запит.
	 * @param URL - String, адреса еленту PageRouter.
	 * @return link - String, повний URL з урахуванням ROOT серверу.
	 */
	public static final String createLink(HttpServletRequest request, String URL) {
		return request.getContextPath() + URL;
	}
}
