<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@page import="classes.com.my.tools.navigation.PageRouter"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<c:set var="mainPageLink" value="<%= PageRouter.createLink(request, PageRouter.MAIN.getURL()) %>" />
<c:set var="registrationPageLink" value="<%= PageRouter.createLink(request, PageRouter.REGISTRATION.getURL()) %>" />
<c:set var="authorizationPageLink" value="<%= PageRouter.createLink(request, PageRouter.AUTHORISATION.getURL()) %>" />


<title>${sessionScope[Localizer].getValue("main_page_title")}</title>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container-fluid">
		<a class="navbar-brand" href="${mainPageLink}">
			${sessionScope[Localizer].getValue("header_title")}
		</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
			data-bs-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				<li class="nav-item">
					<a class="nav-link active"
					aria-current="page" 
					href="${mainPageLink}">
						${sessionScope[Localizer].getValue("header_schedule")}
					</a>
				</li>
			</ul>
			<ul class="navbar-nav justify-content-end mr-2">
				<li class="nav-item">
					<a class="nav-link" href="${registrationPageLink}">
						${sessionScope[Localizer].getValue("header_registration")}
					</a>
				</li>
				<li class="nav-item">
					<p class="text">|</p>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="${authorizationPageLink}">
						${sessionScope[Localizer].getValue("header_sign_in")}
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>