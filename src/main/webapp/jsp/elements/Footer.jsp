<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
	<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
   
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<c:set var="LangList" value="<%= GlobalAttributes.SESSION_ATTR_FOOTER_LANG_AE %>" />

<footer class="footer">
<nav class="navbar mt-5" style="background-color: #212529; color:#fff">
  <div class="row mt-2 m-4">
  	<div class="col">
  		<img style="max-width: 50%" alt="Cinema logo" src="https://cdn4.iconfinder.com/data/icons/ballicons-2-new-generation-of-flat-icons/100/cinema-256.png">
  		<p>${sessionScope[Localizer].getValue("footer_all_rights")}</p>
  		
  		<form method="GET" action="/maven-web/lang">
  			<p>${sessionScope[Localizer].getValue("footer_lang")}
	  			<select onchange="this.form.submit()" name="lng">
	  				
	  				<c:forEach var="lang" items="${sessionScope[LangList]}">
		  				
		  				<option 
		  					<c:if test="${lang.getValue().equals(sessionScope[Localizer].getLang())}">
		  						<c:out value="selected"/>
		  					</c:if>
		  					value="${lang.getValue()}" 
		  				>
			  				${lang.getLabel()}
			  			</option>
			  			
	  				</c:forEach>
	  				
		  		</select>
  			</p>
  		</form>
  		 
  	</div>
  	<div class="col">
  		<p>${sessionScope[Localizer].getValue("footer_navigation")}</p>
  		<a href="" style="color: grey;">${sessionScope[Localizer].getValue("footer_schedule")}</a>
  	</div>
  </div>
</nav>
</footer>
