<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<title>${sessionScope[Localizer].getValue("main_page_title")}</title>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container-fluid">
		<a class="navbar-brand" href="/maven-web">
			${sessionScope[Localizer].getValue("header_title")}
		</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
			data-bs-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				<li class="nav-item">
					<a class="nav-link active"
					aria-current="page" 
					href="/maven-web">
						${sessionScope[Localizer].getValue("header_schedule")}
					</a>
				</li>
			</ul>
			<ul class="navbar-nav justify-content-end mr-2">
				<li class="nav-item">
					<a class="nav-link" href="/maven-web/orders">
						Сart
					</a>
				</li>
				<li class="nav-item">
					<p class="text">|</p>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/maven-web/profile">
						Profile
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>