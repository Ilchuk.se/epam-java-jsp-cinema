<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@page import="classes.com.my.tools.navigation.PageRouter"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
	crossorigin="anonymous">

<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
	crossorigin="anonymous"></script>
</head>
<body style="min-height: 100%">
	<c:set var="user" value="<%= GlobalAttributes.SESSION_ATTR_CUR_USER %>"></c:set>
	<c:choose>
		<c:when test="${sessionScope[user] == null}">
			<jsp:include page="<%= PageRouter.HEADER_DEFFAULT.getPath() %>"></jsp:include>
		</c:when>
		<c:otherwise>
			<jsp:include page="<%= PageRouter.HEADER_ATHORIZED.getPath() %>"></jsp:include>
		</c:otherwise>
	</c:choose>

	<c:if test="${innerPage != null}">
		<jsp:include page="${innerPage}"></jsp:include>
	</c:if>
	
	<jsp:include page="<%= PageRouter.FOOTER.getPath() %>"></jsp:include>
</body>
</html>