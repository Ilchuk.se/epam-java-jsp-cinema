
<%@page import="classes.com.my.controllers.page.profile.elements.ProfileStatisticsPageController"%>
<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@page import="classes.com.my.tools.navigation.PageRouter"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:set var="fileName" value="<%= ProfileStatisticsPageController.REQUEST_ATTR_PDF_FILE_NAME %>" />
<c:set var="avrStats" value="<%= ProfileStatisticsPageController.REQUEST_ATTR_HALL_STATS %>" />

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<c:set var="Offers" value="<%= ProfileStatisticsPageController.REQUEST_ATTR_ALL_OFFERS %>" />

<div class="container justify-content-center">
	<div class="row">
		<h2 class="text-center m-2">Hall statistics</h2>
		
		<form method="post" action="">
			<button class="btn btn-primary" type="submit" name="<%= ProfileStatisticsPageController.REQUEST_ATTR_PDF_FILE_NAME %>" value="${requestScope[fileName]}">Download as PDF</button>
		</form>
		
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Hall</th>
					<th>Film</th>
					<th>Date</th>
					<th>Time</th>
					<th>Price</th>
					<th>Places</th>
					<th>Orders</th>
					<th>Occupancy</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="offer" items="${requestScope[Offers]}">
					<tr>
						<td>${offer.getOfferId()}</td>
						<td>${offer.getHallLocalizedName()}</td>
						<td>${offer.getFilmLocalizedName()}</td>
						<td>${offer.getOfferDate()}</td>
						<td>${offer.getOfferTime()}</td>
						<td>${offer.getOfferPrice()}</td>
						<td>${offer.getHallSize()}</td>
						<td>${offer.getOrderUnitsOrdered()}</td>
						<td>
							<fmt:formatNumber
							  value="${ offer.getOrderUnitsOrdered() / offer.getHallSize() * 100.0 }"
							  maxFractionDigits="2" />
							  %
						</td>
					</tr>
				</c:forEach>
			 </tbody>
		</table>
		
		<c:if test="${requestScope[avrStats] != null}">
			<h2 class="text-center m-2">Total statistics</h2>
			<p>Current offers: ${requestScope[avrStats].getOffersNum()}</p>
			<br/>
			<p>Places: ${requestScope[avrStats].getPlacesNum()}</p>
			<br/>
			<p>Orders: ${requestScope[avrStats].getOrdersNum()}</p>
			<br/>
			<p>Average occupancy: ${requestScope[avrStats].getOccupancy()} %</p>
		</c:if>
		
	</div>
</div>
