<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<c:set var="User" value="<%= GlobalAttributes.SESSION_ATTR_CUR_USER %>" />

<h2>User info</h2>
<div class="row">
	<p>User nickname: ${sessionScope[User].getUserNickname()}</p>
	<p>User ID: ${sessionScope[User].getUserId()}</p>
	<form method="POST" action="">
		<button type="submit" class="btn btn-danger mb-3">Sign out</button>
	</form>
</div>