<%@page import="classes.com.my.controllers.page.profile.elements.ProfileOfferPageController"%>
<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<c:set var="Offers" value="<%= ProfileOfferPageController.REQUEST_ATTR_ALL_OFFERS %>"/>

<div class="container justify-content-center">
	<div class="row">
		<h2 class="text-center m-2">All offers</h2>
		<a href="/maven-web/profile/offer/new" class="link-primary m-3">+ add new offer</a>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Original name</th>
					<th>Localized name</th>
					<th>Date</th>
					<th>Time</th>
					<th>Price</th>
					<th>Hall</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="offer" items="${requestScope[Offers]}">
					<tr>
						<td>${offer.getOfferId()}</td>
						<td>${offer.getFilmOriginalName()}</td>
						<td>${offer.getFilmLocalizedName()}</td>
						<td>${offer.getOfferDate()}</td>
						<td>${offer.getOfferTime()}</td>
						<td>${offer.getOfferPrice()}</td>
						<td>${offer.getHallLocalizedName()}</td>
						<td>
							<a href="#">Delete</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>