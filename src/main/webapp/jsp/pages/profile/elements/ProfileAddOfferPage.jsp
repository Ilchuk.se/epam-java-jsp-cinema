<%@page import="classes.com.my.controllers.page.profile.elements.ProfileAddOfferPageController"%>
<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<c:set var="HallList" value="<%= ProfileAddOfferPageController.REQUEST_ATTR_HALL_LOCALIZED_LIST %>" />
<c:set var="FilmList" value="<%= ProfileAddOfferPageController.REQUEST_ATTR_FILM_LOCALIZED_LIST %>" />

<div class="container">

	<h2 class="text-center m-3">New offer</h2>
	
	<form method="POST" action="">
		<div class="row m-2">
			<div class="col-2">
				<p>Hall: </p>
			</div>
			<div class="col-10">
				<select class="form-select" name="<%= ProfileAddOfferPageController.REQUEST_ATTR_HALL_ID %>">
					<c:forEach var="hall" items="${requestScope[HallList]}">
						<option value="${hall.getId()}">Id: ${hall.getId()}, Name: ${hall.getName()}, size: ${hall.getSize()}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="row m-2">
			<div class="col-2">
				<p>Film: </p>
			</div>
			<div class="col-10">
				<select class="form-select" name="<%= ProfileAddOfferPageController.REQUEST_ATTR_FILM_ID %>">
					<c:forEach var="film" items="${requestScope[FilmList]}">
						<option value="${film.getId()}">Id: ${film.getId()}, Name: ${film.getLocalizedName()}, orig.(${film.getName()})</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="row m-2">
			<div class="col-2">
				<p>Date: </p>
			</div>
			<div class="col-10">
				<input type="date" class="form-control" name="<%= ProfileAddOfferPageController.REQUEST_ATTR_DATE %>">
			</div>
		</div>
		
		<div class="row m-2">
			<div class="col-2">
				<p>Time: </p>
			</div>
			<div class="col-10">
				<input type="time" class="form-control" name="<%= ProfileAddOfferPageController.REQUEST_ATTR_TIME %>">
			</div>
		</div>
		
		<div class="row m-2">
			<div class="col-2">
				<p>Price: </p>
			</div>
			<div class="col-10">
				<input type="number" class="form-control" min="1" step="any" name="<%= ProfileAddOfferPageController.REQUEST_ATTR_PRICE %>"/>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary mb-3">
			Add									
		</button>
	</form>
</div>