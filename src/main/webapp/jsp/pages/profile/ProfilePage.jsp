<%@page import="classes.com.my.controllers.UserRights"%>
<%@page import="classes.com.my.tools.dbmanager.viewmanagers.entity.UserData"%>
<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@page import="classes.com.my.controllers.BaseController"%>
<%@page import="classes.com.my.tools.navigation.PageRouter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
 <c:set var="infoLink" value="<%= PageRouter.createLink(request, PageRouter.PROFILE_INFO.getURL()) %>"></c:set>
 <c:set var="filmLink" value="<%= PageRouter.createLink(request, PageRouter.PROFILE.getURL()) %>"></c:set>
 <c:set var="offerLink" value="<%= PageRouter.createLink(request, PageRouter.PROFILE_OFFER.getURL()) %>"></c:set>
 <c:set var="statLink" value="<%= PageRouter.createLink(request, PageRouter.PROFILE_STATISTICS.getURL()) %>"></c:set>
 
 <div class="container">
 	<div class="row" style="min-height: 300px;">
 		<div class=col-3>
	 		<div class="card mt-3" style="width: 18rem;">
		 		<a href="${infoLink}" class="link-primary m-2">User info</a>
		 		
		 		<c:if test="<%= ((UserData)session.getAttribute(GlobalAttributes.SESSION_ATTR_CUR_USER)).getSystemRoleId() == UserRights.ADMIN.getId() %>">
		 			<a href="${filmLink}" class="link-primary m-2">Films</a>
			 		<a href="${offerLink}" class="link-primary m-2">Offers</a>
			 		<a href="${statLink}" class="link-primary m-2">Hall statistics</a>
		 		</c:if>
		 		
	 		</div>
	 	</div>	
	 	<div class="col-9">
	 		<div class="card m-3 w-100 justify-content-center align-items-center" style="width: 18rem;" >
	 			<c:if test="${profileInnerPage != null}">
		 			<jsp:include page="${profileInnerPage}"></jsp:include>
		 		</c:if>
	 		</div>
	 	</div>
 	</div>
 </div>