
<%@page import="classes.com.my.controllers.page.filmdescription.FilmOfferPageController"%>
<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
	<c:set var="film" value="<%= FilmOfferPageController.REQUEST_ATTR_FILM %>" />	
	
	<div class="row m-3">
		<div class="col-3">
			<img class="img-fluid" alt="Film poster" src="${requestScope[film].getFilmPosterImgDir()}">
		</div>
		<div class="col-9">	
			<h1>
				${requestScope[film].getFilmLocalizedName()}
			</h1>
			<p>
				${sessionScope[Localizer].getValue("offer_page_orig")}
				 .( 
				 ${requestScope[film].getFilmOriginalName()} )
			</p>
			<p>
				${sessionScope[Localizer].getValue("offer_page_time")} ${requestScope[film].getFilmTimekeeping()}
			</p>
			<p>
				${sessionScope[Localizer].getValue("offer_page_hall")} ${requestScope[film].getHallLocalizedName()}
			</p>
			<p>
				${sessionScope[Localizer].getValue("offer_page_date")} ${requestScope[film].getOfferDate()}
			</p>
			<p>
				${sessionScope[Localizer].getValue("offer_page_time")} ${requestScope[film].getOfferTime()}
			</p>
			<p>
				${sessionScope[Localizer].getValue("offer_page_places")} ${requestScope[film].getOrderUnitsOrdered()} from ${requestScope[film].getHallSize()}
			</p>
			<p>
				Price: ${requestScope[film].getOfferPrice()} hrn.
			</p>
			<p>
				${sessionScope[Localizer].getValue("offer_page_description")} ${requestScope[film].getFilmLocalizedDescription()}
			</p>
		</div>
	</div>
	<div style="background-color:#b0b0b0;">
		<div class="row m-5 justify-content-center">
		<h3 class="mt-4">${sessionScope[Localizer].getValue("offer_page_hall_scheme")}</h3>
		<img class="img-fluid mb-3 mt-3" style="max-width: 70%; align: center;" alt="Cinema hall scheme" src="https://www.goldmir.net/uploads/kino/cinema/180/scheme_437.jpg"/>
			
			<form method="POST" action="">
				<table class="table"> 
					<tr>
						<h4 class="mt-4">${sessionScope[Localizer].getValue("offer_page_select_place")}</h4>
					</tr>
					<tr>
						<th>${sessionScope[Localizer].getValue("offer_page_places")}</th>
					</tr>
					<c:set var="boockedPlaces"
						value="<%=FilmOfferPageController.REQUEST_ATTR_BOOCKED_PLACES%>"></c:set>
					<c:forEach var="i" begin="1"
						end="${requestScope[film].getHallSize()}">
						<c:if test="${i % 10 == 1}">
							<tr>
						</c:if>
	
						<th>${i} <c:choose>
								<c:when test="${requestScope[boockedPlaces].contains(i)}">
									<img class="img-fluid" style="max-width: 10%;" alt="free"
										src="https://upload.wikimedia.org/wikipedia/commons/f/f1/Ski_trail_rating_symbol_red_circle.png">
								</c:when>
								<c:otherwise>
									<img class="img-fluid" style="max-width: 10%;" alt="free"
										src="https://upload.wikimedia.org/wikipedia/commons/1/11/Pan_Green_Circle.png">
									
									<c:set var="offerDone"
										value="<%=FilmOfferPageController.REQUEST_ATTR_IS_OFFER_DONE %>" />
									
									<c:if test="${requestScope[offerDone] != null}">
										<input type="checkbox" value="${i}" name="sp[]">
									</c:if>
								</c:otherwise>
							</c:choose>
						</th>
	
						<c:if test="${i % 10 == 0}">
							</tr>
						</c:if>
					</c:forEach>
				</table>
				<br/>
				<c:if test="${requestScope[offerDone] != null}">
					<button class="btn-primary w-25 mb-4" type="submit">${sessionScope[Localizer].getValue("offer_page_button_buy")}</button>
				</c:if>
			</form>
		</div>
	</div>
</body>
</html>