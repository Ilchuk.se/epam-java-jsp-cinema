<%@page import="classes.com.my.controllers.FrontFilter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<div class="container">
	<div class="row justify-content-center align-items-center">
		<div class="card w-50 mb-4 mt-4 text-center">
			<article class="card-body">
				<h4 class="card-title text-center mb-4 mt-1">Oops, something goes wrong!</h4>
				<p class="text text-center"><%= request.getAttribute(FrontFilter.REQUEST_ATTR_EXCEPTION) %></p>
				<a class="btn btn-primary btn-center" href="<%= request.getRequestURL().toString() %>" role="button">Go back</a> 
			</article>
		</div>
	</div>
</div>