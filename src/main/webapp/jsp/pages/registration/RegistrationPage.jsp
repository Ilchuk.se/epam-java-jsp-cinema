<%@page import="classes.com.my.controllers.page.registration.RegistrationPageController"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
	<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
	<%@page import="classes.com.my.tools.navigation.PageRouter"%>
	<%@page import="classes.com.my.controllers.page.registration.RegistrationPageController"%>
   
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
<c:set var="registrationPageLink" value="<%= PageRouter.createLink(request, PageRouter.REGISTRATION.getURL()) %>" />
<c:set var="userMessage" value="<%= RegistrationPageController.REQUEST_ATTR_MESSAGE %>" />

<c:set var="name" value="<%= RegistrationPageController.REGUEST_POST_ATTR_NAME %>" />
<c:set var="login" value="<%= RegistrationPageController.REGUEST_POST_ATTR_LOGIN %>" />
<c:set var="pass" value="<%= RegistrationPageController.REGUEST_POST_ATTR_PASSWORD %>" />
<c:set var="passDup" value="<%= RegistrationPageController.REGUEST_POST_ATTR_PASSWORD_DUP %>" />

<div class="container">
	<div class="row justify-content-center align-items-center">
		<div class="card w-50 mb-4 mt-4">
			<article class="card-body">
				<h4 class="card-title text-center mb-4 mt-1">${sessionScope[Localizer].getValue("reg_page_registration")}</h4>
				<hr>
				<p class="text-danger text-center">${requestScope[userMessage]}</p>
				<form method="POST" action="${registrationPageLink}">
					<div class="row">
						<div class="col-4">
							<span class="input-group-text">${sessionScope[Localizer].getValue("reg_page_nick")}</span>
						</div>
						<div class="col-8">
							<input name="${name}" class="form-control" placeholder='${sessionScope[Localizer].getValue("reg_page_palceholder_nick")}'
								type="text">
						</div>
					</div>
					<br />

					<div class="row">
						<div class="col-4">
							<span class="input-group-text">${sessionScope[Localizer].getValue("reg_page_login")}</span>
						</div>
						<div class="col-8">
							<input name="${login}" class="form-control"
								placeholder='${sessionScope[Localizer].getValue("reg_page_placeholder_login")}' type="text">
						</div>
					</div>
					<br />

					<div class="row">
						<div class="col-4">
							<span class="input-group-text">${sessionScope[Localizer].getValue("reg_page_password")}</span>
						</div>
						<div class="col-8">
							<input name="${pass}" class="form-control" placeholder='${sessionScope[Localizer].getValue("reg_page_placeholder_password")}'
								type="password">
						</div>
					</div>
					<br />

					<div class="row">
						<div class="col-4">
							<span class="input-group-text">${sessionScope[Localizer].getValue("reg_page_duplicate_password")} </span>
						</div>
						<div class="col-8">
							<input name="${passDup}" class="form-control" placeholder='${sessionScope[Localizer].getValue("reg_page_placeholder_password")}'
								type="password">
						</div>
					</div>
					<br />

					<button type="submit" class="btn btn-primary btn-block">
						${sessionScope[Localizer].getValue("reg_page_button_register")}</button>
				</form>
			</article>
		</div>
	</div>
</div>
