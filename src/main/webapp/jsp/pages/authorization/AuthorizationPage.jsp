
	<%@page import="classes.com.my.controllers.page.authorization.AuthorizarionPageController"%>
<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">

	<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />
	
	<div class="row justify-content-center align-items-center">
		<div class="card w-50" style="margin-top: 5%; margin-bottom: 5%;">
			<article class="card-body">
				<h4 class="card-title text-center mb-4 mt-1">${sessionScope[Localizer].getValue("auth_page_sign_in")}</h4>
				<hr>
				<c:set var="message"
					value="<%= AuthorizarionPageController.REQUEST_ATTR_VIEW_LOG_IN_MESSAGE%>" />
				<p class="text-danger text-center">${requestScope[message]}</p>
				<form method="POST" action="authorization">
					<div class="row">
						<div class="col-4">
							<span class="input-group-text"> ${sessionScope[Localizer].getValue("auth_page_login")} </span>
						</div>
						<div class="col-8">
							<input
								name="<%=AuthorizarionPageController.REQUEST_ATTR_USER_LOGIN%>"
								class="form-control" 
								placeholder='${sessionScope[Localizer].getValue("auth_page_placeholder_login")}' 
								type="text"
							>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-4">
							<span class="input-group-text"> ${sessionScope[Localizer].getValue("auth_page_password")} </span>
						</div>
						<div class="col-8">
							<input
								name="<%=AuthorizarionPageController.REQUEST_ATTR_USER_PASSWORD%>"
								class="form-control" 
								placeholder='${sessionScope[Localizer].getValue("auth_page_placeholder_password")} ' 
								type="password">
						</div>
					</div>
					<br />
					<button type="submit" class="btn btn-primary btn-block">
						${sessionScope[Localizer].getValue("auth_page_button_login")}
					</button>
				</form>
			</article>
		</div>
	</div>
</div>