<%@page import="classes.com.my.controllers.page.cart.OrdersPageController"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container m-4 justify-content-center"
	style="min-height: 400px">
	<h2>Orders</h2>
	<table class="table">
		<thead>
			<tr>
				<th>Ticket number</th>
				<th>Date</th>
				<th>Time</th>
				<th>Hall</th>
				<th>Place</th>
				<th>Film</th>
				<th>Price</th>
				<th>More info</th>
			</tr>
		</thead>
		
		<tbody>
			<c:set var="userOrders"
				value="<%= OrdersPageController.REQUEST_ATTR_CUR_USER_ORDER %>" />
			<c:forEach var="order" items="${requestScope[userOrders]}">
					<tr>
						<td>${order.getId()}</td>
						<td>${order.getDate()}</td>
						<td>${order.getTime()}</td>
						<td>${order.getHallName()}</td>
						<td>${order.getPlace()}</td>
						<td>${order.getFilmName()}</td>
						<td>${order.getPrice()}</td>
						<td><a href="/maven-web/film?id=${order.getOfferId()}">more</a></td>
					</tr>
			</c:forEach>
		</tbody>

	</table>
</div>