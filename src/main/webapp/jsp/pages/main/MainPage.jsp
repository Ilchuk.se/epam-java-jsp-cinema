<%@page import="classes.com.my.controllers.page.main.MainPageController"%>
<%@page import="classes.com.my.tools.attributes.GlobalAttributes"%>
<%@page import="classes.com.my.tools.navigation.PageRouter"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="Localizer" value="<%= GlobalAttributes.SESSION_LOCALIZER %>" />

<div class="container">
	<div class="row mt-3 justify-content-center">
		<h2 class="text-center m-2">
			${sessionScope[Localizer].getValue("main_page_head_all_offers")}</h2>
		<p class="text-center">
			${sessionScope[Localizer].getValue("main_page_head_everything")}</p>
	</div>
	<div class="row mb-2">
		<div class="col-9">
		</div>
		<div class="col-3">
			<form method="GET" action="">
				<select class="form-select w-100" name="<%= MainPageController.REQUEST_GET_ATTR_SORT %>"
					onchange="this.form.submit()">
					
					<c:set var="SortAll"
						value="<%= MainPageController.REQUEST_ATTR_VIEW_SORT_ALL %>" />
						
					<c:set var="SortSelected"
						value="<%= MainPageController.SESSION_ATTR_VIEW_SORT_SELECTED %>" />
					
					<c:forEach var="sort" items="${requestScope[SortAll]}">
						<option
							<c:if test="${sort.getValue().equals(sessionScope[SortSelected])}">
								<c:out value="selected"/>
							</c:if>
							value="${sort.getValue()}">
							<c:out value="${sort.getLabel()}" />
						</option>
					</c:forEach>
					
				</select>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-3">
			<div class="card">
				<article class="card-group-item">
					<header class="card-header">
						<h6 class="title">
							${sessionScope[Localizer].getValue("main_page_choose_date")}</h6>
					</header>

					<form method="GET" onchange="">
						<div class="filter-content">
							<div class="card-body">

								<c:set var="DateFiltersAll"
									value="<%= MainPageController.REQUEST_ATTR_VIEW_FILTER_DATE_ALL %>" />
								<c:set var="DateFilterSelected"
									value="<%= MainPageController.SESSION_ATTR_VIEW_FILTER_DATE_SELECTED %>" />
								
								<c:forEach var="filter" items="${requestScope[DateFiltersAll]}">
									<label class="form-check"> <input
										class="form-check-input" onchange="this.form.submit()"
										type="radio"
																		
										<c:if test="${filter.getValue().equals(sessionScope[DateFilterSelected])}">
											<c:out value=" checked "/>
										</c:if>
										name="<%= MainPageController.REQUEST_GET_ATTR_FILTER_DATE %>" value="${filter.getValue()}"> <span
										class="form-check-label"> 
											<c:out value="${filter.getLabel()}" />
									</span>
									</label>
								</c:forEach>

							</div>
						</div>
					</form>

				</article>

				<article class="card-group-item">
					<header class="card-header">
						<h6 class="title">
							${sessionScope[Localizer].getValue("main_page_films")}</h6>
					</header>
					<div class="filter-content">
						<div class="card-body">
							<form method="GET" onchange="">
								<c:set var="FilmFiltersAll"
									value="<%= MainPageController.REQUEST_ATTR_VIEW_FILTER_FILMS_ALL %>" />
								<c:set var="FilmFiltersSelected"
									value="<%= MainPageController.SESSION_ATTR_VIEW_FILTER_FILMS_SELECTED %>" />
								
								<c:forEach var="film" items="${requestScope[FilmFiltersAll]}">
									<label class="form-check"> 
										<input
											class="form-check-input" 
											type="checkbox" 
											name="<%= MainPageController.REQUEST_GET_ATTR_FILTER_FILM %>"
											value="${film.getValue()}" 
											onchange="this.form.submit()"
											<c:if test="${sessionScope[FilmFiltersSelected].contains(film.getValue())}">
												<c:out value=" checked "/>
											</c:if>
										>
										<span class="form-check-label"> <c:out
												value="${film.getLabel()}" />
									</span>
									</label>
								</c:forEach>

							</form>
						</div>
						<!-- card-body.// -->
					</div>
				</article>
			</div>
		</div>
		<div class="col-9">
			<div class="row">
			
				<form method="GET" action="<%= request.getContextPath() + PageRouter.FILM_DESCRIPTION.getURL() %>">
					
					<div class="card-group">
						
						<c:set var="OffersAll"
							value="<%= MainPageController.REQUEST_ATTR_OFFERS_DISPLAYED %>" />
						<c:forEach var="offer" items="${requestScope[OffersAll]}">
							
							<div class="card" style="min-width: 33%; max-width: 33%;">
								<div class="card-body">
									<h5 class="card-title">
										<c:out value="${offer.getFilmLocalizedName()}"></c:out>
									</h5>
									<p class="card-text" style="font-size: 10pt;">
										${sessionScope[Localizer].getValue("main_page_card_orig")}(
										<c:out value="${offer.getFilmOriginalName()}">
										</c:out>
										)
									</p>
									<p class="card-text">
										${sessionScope[Localizer].getValue("main_page_card_date")}
										<c:out value="${offer.getOfferDate()}">
										</c:out>
									</p>
									<p class="card-text">
										${sessionScope[Localizer].getValue("main_page_card_time")}
										<c:out value="${offer.getOfferTime()}">
										</c:out>
									</p>
									<p class="card-text">
										${sessionScope[Localizer].getValue("main_page_card_hall")}
										<c:out value="${offer.getHallLocalizedName()}">
										</c:out>
									</p>
									<p class="card-text">
										${sessionScope[Localizer].getValue("main_page_card_empty")}
										<c:out
											value="${offer.getHallSize() - offer.getOrderUnitsOrdered()}" />
										${sessionScope[Localizer].getValue("main_page_card_from")}
										<c:out value="${offer.getHallSize()}" />
									</p>
									
										<button type="submit" value="${offer.getOfferId()}" name="id"
											class="btn btn-primary">
											${sessionScope[Localizer].getValue("main_page_card_button")}</button>
								</div>
							</div>
						</c:forEach>
					</div>
					
				</form>
				
				<ul class="pagination pagination-mg m-3 justify-content-center">
					
					<c:set var="allPagesNum"
						value="<%= MainPageController.REQUEST_ATTR_ALL_PAGE_COUNT %>" />
							
					<c:set var="currentPage"
						value="<%= MainPageController.REQUEST_ATTR_PAGE_NUM %>" />
							
					<c:set var="pageLink" 
						value='<%= PageRouter.createLink(request, PageRouter.MAIN.getURL()) + "?" + MainPageController.REQUEST_GET_ATTR_PAGE +"=" %>'/>
					
					<c:forEach var="page" begin="1" end="${requestScope[allPagesNum]}">
						<li class='page-item
							<c:if test="${requestScope[currentPage].equals(String.valueOf(page))}">
								active
							</c:if>
						'
						>
							<a href="${pageLink.concat(page)}" class="page-link">${page}</a>
						</li>
					</c:forEach>
				</ul>
				
			</div>
		</div>
	</div>
</div>