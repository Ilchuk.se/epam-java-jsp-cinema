package classes.com.my.listeners.session;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.junit.Test;

import com.mysql.cj.Session;

import classes.com.my.tools.attributes.GlobalAttributes;

public class SessionListenerTest {
	
	@Test
	public void testSessionCreated() {
		SessionListener sessionListener = new SessionListener();
		HttpSessionEvent se = mock(HttpSessionEvent.class);
		HttpSession session = mock(HttpSession.class);
		
		when(session.getAttribute(GlobalAttributes.SESSION_ATTR_LOCALE)).thenReturn(null);
		when(session.getAttribute(GlobalAttributes.SESSION_LOCALIZER)).thenReturn(null);
		
		when(se.getSession()).thenReturn(session);
		
		sessionListener.sessionCreated(se);
	}
	
	@Test
	(expected = Exception.class)
	public void testSessionCreatedWithException() {
		SessionListener sessionListener = new SessionListener();
		sessionListener.sessionCreated(null);
	}
	
	@Test
	public void testSessionDestroyed() {
		SessionListener sessionListener = new SessionListener();
		HttpSessionEvent se = mock(HttpSessionEvent.class);
		sessionListener.sessionDestroyed(se);
		assertTrue(true);
	}
}
