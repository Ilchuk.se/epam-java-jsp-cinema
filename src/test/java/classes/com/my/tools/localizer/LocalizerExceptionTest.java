package classes.com.my.tools.localizer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LocalizerExceptionTest {
	@Test
	public void testConstructor() {
		final Exception ex = new Exception("Some reason");
		final String message = "some message";
		final LocalizerException exception = new LocalizerException(message, ex);
		
		assertEquals(exception.getMessage(), message);
		assertEquals(exception.getCause(), ex);
	}
}
