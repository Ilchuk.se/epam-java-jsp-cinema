package classes.com.my.tools.localizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

public class LocalizerTest {
	private Localizer localizer;
	private final String lang = "en";
	private final String bundleName = "resources";
	private final String tagName = "main_page_sort_by_name";
	private final String tagValue = "Name";
	
	@Before
	public void setUp() throws Exception {
		Locale locale = new Locale(lang);
		localizer = new Localizer(bundleName, locale);
	}
	
	@Test
	public void testGetLang() {		
		assertEquals(lang, localizer.getLang());
	}
	
	@Test
	public void testGetValue() {
		String value = localizer.getValue(tagName);
		assertEquals(value, tagValue);
	}
	
	@Test
	(expected = LocalizerException.class)
	public void testConstructorWithNotExistingBundle() throws LocalizerException {
		String bunndle = "not existing bindle";
		Localizer loc = new Localizer(bunndle, null);
	}
	
	@Test
	public void testGetValueWithNotExistingTag(){
		String val = localizer.getValue("not existing tag");
		assertNull(val);
	}
}
