package classes.com.my.tools.dbmanager.viewmanagers.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HallLocalizedTest {
	private final int id = 1;
	private final int size = 12;
	private final String name = "hallName";
	private HallLocalized hallLocalized = new HallLocalized();
	
	@Test
	public void testSetGetId() {
		hallLocalized.setId(id);
		assertEquals(hallLocalized.getId(), id);
	}
	
	@Test
	public void testSetGetName() {
		hallLocalized.setName(name);;
		assertEquals(hallLocalized.getName(), name);
	}
	
	@Test
	public void testSetGetSize() {
		hallLocalized.setSize(size);
		assertEquals(hallLocalized.getSize(), size);
	}
}
