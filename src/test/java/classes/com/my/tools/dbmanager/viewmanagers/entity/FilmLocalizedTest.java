package classes.com.my.tools.dbmanager.viewmanagers.entity;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import classes.com.my.tools.localizer.Localizer;

public class FilmLocalizedTest {
	private final int id = 1;
	private final String name = "someName";
	private final String localizedName = "locName";
	private FilmLocalized filmLocalized = new FilmLocalized();
	
	@Test
	public void testSetGetId() {
		filmLocalized.setId(id);
		assertEquals(filmLocalized.getId(), id);
	}
	
	@Test
	public void testSetGetName() {
		filmLocalized.setName(name);;
		assertEquals(filmLocalized.getName(), name);
	}
	
	@Test
	public void testSetGetLocalizedName() {
		filmLocalized.setLocalizedName(localizedName);;
		assertEquals(filmLocalized.getLocalizedName(), localizedName);
	}
}
