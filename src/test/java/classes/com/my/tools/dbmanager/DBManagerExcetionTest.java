package classes.com.my.tools.dbmanager;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DBManagerExcetionTest {
	@Test
	public void testConstructor() {
		final Exception ex = new Exception("Some reason");
		final String message = "some message";
		final DBManagerException exception = new DBManagerException(message, ex);
		
		assertEquals(exception.getMessage(), message);
		assertEquals(exception.getCause(), ex);
	}
}
