package classes.com.my.tools.dbmanager.tablemanagers.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OrderTest {
	private int ticketId = 3;
	private int userId = 7;
	private int place = 10;
	private boolean isPaid = true;
	
	private Order order = new Order();
	
	@Test
	public void testSetGetId() {
		order.setTicketId(ticketId);
		assertEquals(order.getTicketId(), ticketId);
	}
	
	@Test
	public void testSetUserId() {
		order.setUserId(userId);
		assertEquals(order.getUserId(), userId);
	}
	
	@Test
	public void testSetPlace() {
		order.setPlace(place);
		assertEquals(order.getPlace(), place);
	}
	
	@Test
	public void testSetIsPaid() {
		order.setPaid(isPaid);
		assertEquals(order.isPaid(), isPaid);
	}
}
