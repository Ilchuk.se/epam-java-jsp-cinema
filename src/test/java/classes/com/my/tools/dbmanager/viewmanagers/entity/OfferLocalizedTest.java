package classes.com.my.tools.dbmanager.viewmanagers.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

import org.junit.Test;

public class OfferLocalizedTest {
	private final int offerId = 12;
	private final Date offerDate = new Date(212121);
	private final Time offerTime = new Time(2121);
	private final BigDecimal offerPrice = BigDecimal.valueOf(13.2);

	private final int languageId = 24;
	private final String languageName = "en";

	private final int filmId = 67;
	private final String filmOriginalName = "someName";
	private final Time filmTimekeeping = new Time(43534);
	private final String filmPosterImgDir = "/dir/image.png";
	private final String filmLocalizedName = "якась назва";
	private final String filmLocalizedDescription = "якийсь опис фільму";

	private final int hallId = 78;
	private final int hallSize =52;
	private final String hallSchemeImgDir = "/dir/scheme.png";
	private final String hallLocalizedName = "якийсь зал";
	
	private final int orderUnitsOrdered = 7;
	
	private OfferLocalized offerLocalized = new OfferLocalized();
	
	@Test
	public void testStaticInit() {
	    assertNotNull(OfferLocalized.getOffer());	
	}
	
	@Test
	public void testSetGetOfferId() {
		offerLocalized.setOfferId(offerId);
		assertEquals(offerLocalized.getOfferId(), offerId);
	}
	
	@Test
	public void testSetGetOfferDate() {
		offerLocalized.setOfferDate(offerDate);
		assertEquals(offerLocalized.getOfferDate(), offerDate);
	}
	
	@Test
	public void testSetGetOfferTime() {
		offerLocalized.setOfferTime(offerTime);
		assertEquals(offerLocalized.getOfferTime(), offerTime);
	}
	
	@Test
	public void testSetGetOfferPrice() {
		offerLocalized.setOfferPrice(offerPrice);
		assertEquals(offerLocalized.getOfferPrice(), offerPrice);
	}
	
	@Test
	public void testSetGetLanguageId() {
		offerLocalized.setLanguageId(languageId);
		assertEquals(offerLocalized.getLanguageId(), languageId);
	}
	
	@Test
	public void testSetGetLanguageName() {
		offerLocalized.setLanguageName(languageName);
		assertEquals(offerLocalized.getLanguageName(), languageName);
	}
	
	@Test
	public void testSetGetFilmId() {
		offerLocalized.setFilmId(filmId);
		assertEquals(offerLocalized.getFilmId(), filmId);
	}
	
	@Test
	public void testSetGetFilmOriginalName() {
		offerLocalized.setFilmOriginalName(filmOriginalName);
		assertEquals(offerLocalized.getFilmOriginalName(), filmOriginalName);
	}
	
	@Test
	public void testSetGetFilmTimekeeping() {
		offerLocalized.setFilmTimekeeping(filmTimekeeping);
		assertEquals(offerLocalized.getFilmTimekeeping(), filmTimekeeping);
	}
	
	@Test
	public void testSetGetFilmPosterImgDir() {
		offerLocalized.setFilmPosterImgDir(filmPosterImgDir);
		assertEquals(offerLocalized.getFilmPosterImgDir(), filmPosterImgDir);
	}
	
	@Test
	public void testSetGetFilmLocalizedName() {
		offerLocalized.setFilmLocalizedName(filmLocalizedName);
		assertEquals(offerLocalized.getFilmLocalizedName(), filmLocalizedName);
	}
	
	@Test
	public void testSetGetFilmLocalizedDescription() {
		offerLocalized.setFilmLocalizedDescription(filmLocalizedDescription);
		assertEquals(offerLocalized.getFilmLocalizedDescription(), filmLocalizedDescription);
	}
	
	@Test
	public void testSetGetHallId() {
		offerLocalized.setHallId(hallId);
		assertEquals(offerLocalized.getHallId(), hallId);
	}
	
	@Test
	public void testSetGetHallSize() {
		offerLocalized.setHallSize(hallSize);
		assertEquals(offerLocalized.getHallSize(), hallSize);
	}
	
	@Test
	public void testSetGetHallSchemeImgDir() {
		offerLocalized.setHallSchemeImgDir(hallSchemeImgDir);
		assertEquals(offerLocalized.getHallSchemeImgDir(), hallSchemeImgDir);
	}
	
	@Test
	public void testSetGetHallLocalizedName() {
		offerLocalized.setHallLocalizedName(hallLocalizedName);
		assertEquals(offerLocalized.getHallLocalizedName(), hallLocalizedName);
	}
	
	@Test
	public void testSetGetOrderUnitsOrdered() {
		offerLocalized.setOrderUnitsOrdered(orderUnitsOrdered);
		assertEquals(offerLocalized.getOrderUnitsOrdered(), orderUnitsOrdered);
	}
}
