package classes.com.my.tools.dbmanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class DBParserTest {
	@Test
	public void testTryParseInt() {
		String intString = "1212";
		assertTrue(DBParser.tryParseInt(intString));
	}
	
	@Test
	public void testTryParseIntWhenNotInt() {
		String intString = "some val";
		assertFalse(DBParser.tryParseInt(intString));
	}
	
	@Test
	public void testReplaceTildaWithIntList() {
		final String textWithTilda = "~ some ~ text ~";
		List<Integer> intList = new ArrayList<Integer>(3);
		intList.add(20);
		intList.add(0);
		intList.add(-1);
		
		String expectedTildaReplacement = "20,0,-1";
		
		final String result = DBParser.replaceTildaWithIntList(textWithTilda, intList);
		final String expectedResult = expectedTildaReplacement + " some " 
		+  expectedTildaReplacement + " text " + expectedTildaReplacement;
		
		assertEquals(result, expectedResult);
	}
	
	@Test
	public void testConstructor() {
		DBParser parser = new DBParser();
		assertNotNull(parser);
	}
}
