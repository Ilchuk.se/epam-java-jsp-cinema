package classes.com.my.tools.dbmanager.viewmanagers.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserDataTest {
	private final int userId = 1;
	private final String userNickname = "nick";
	private final int systemRoleId = 2;
	private final String systemRoleName = "user";
	private UserData userData = new UserData();
	
	@Test
	public void testSetGetUserId() {
		userData.setUserId(userId);
		assertEquals(userData.getUserId(), userId);
	}
	
	@Test
	public void testSetGetUserNickname() {
		userData.setUserNickname(userNickname);
		assertEquals(userData.getUserNickname(), userNickname);
	}
	
	@Test
	public void testSetGetSystemRoleId() {
		userData.setSystemRoleId(systemRoleId);
		assertEquals(userData.getSystemRoleId(), systemRoleId);
	}
	
	@Test
	public void testSetGetSystemRoleName() {
		userData.setSystemRoleName(systemRoleName);
		assertEquals(userData.getSystemRoleName(), systemRoleName);
	}
}
