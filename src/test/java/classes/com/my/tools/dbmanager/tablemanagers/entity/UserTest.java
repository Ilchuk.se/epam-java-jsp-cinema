package classes.com.my.tools.dbmanager.tablemanagers.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserTest {
	private final String login = "log";
	private final String password = "pas";
	private final String name = "name";
	private final int roleId = 1;
	
	private User user = new User();
	
	@Test
	public void testSetGetLogin() {
		user.setLogin(login);
		assertEquals(user.getLogin(), login);
	}
	
	@Test
	public void testSetGetPassword() {
		user.setPassword(password);
		assertEquals(user.getPassword(), password);
	}
	
	@Test
	public void testSetGetName() {
		user.setName(name);
		assertEquals(user.getName(), name);
	}
	
	@Test
	public void testSetGetRoleId() {
		user.setRoleId(roleId);
		assertEquals(user.getRoleId(), roleId);
	}
}
