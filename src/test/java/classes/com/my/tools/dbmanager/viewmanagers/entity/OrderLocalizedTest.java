package classes.com.my.tools.dbmanager.viewmanagers.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.sql.Time;

import org.junit.Test;

public class OrderLocalizedTest {
	private final int id = 1;
	private final int place = 2;
	private final Date date = new Date(1212);
	private final Time time = new Time(3434);
	private final String hallName = "name";
	private final String filmName = "some name";
	private final int offerId = 6;
	private final float price = (float)1212.12;
	private OrderLocalized orderLocalized = new OrderLocalized();
	
	@Test
	public void testSetGetId() {
		orderLocalized.setId(id);
		assertEquals(orderLocalized.getId(), id);
	}
	
	@Test
	public void testSetGetPlace() {
		orderLocalized.setPlace(place);
		assertEquals(orderLocalized.getPlace(), place);
	}
	
	@Test
	public void testSetGetDate() {
		orderLocalized.setDate(date);
		assertEquals(orderLocalized.getDate(), date);
	}
	
	@Test
	public void testSetGetTime() {
		orderLocalized.setTime(time);
		assertEquals(orderLocalized.getTime(), time);
	}
	
	@Test
	public void testSetGetHallName() {
		orderLocalized.setHallName(hallName);
		assertEquals(orderLocalized.getHallName(), hallName);
	}
	
	@Test
	public void testSetGetFilmName() {
		orderLocalized.setFilmName(filmName);
		assertEquals(orderLocalized.getFilmName(), filmName);
	}
	
	@Test
	public void testSetGetOfferId() {
		orderLocalized.setOfferId(offerId);
		assertEquals(orderLocalized.getOfferId(), offerId);
	}
	
	@Test
	public void testSetGetPrice() {
		orderLocalized.setPrice(price);
		assertTrue(orderLocalized.getPrice() == price);
	}
}
