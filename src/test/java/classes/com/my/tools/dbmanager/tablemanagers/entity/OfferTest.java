package classes.com.my.tools.dbmanager.tablemanagers.entity;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

import org.junit.Test;

public class OfferTest {
	private final int id = 1;
	private final int filmId = 2;
	private final int hallId = 3;
	private final Date date = new Date(1212);
	private final Time time = new Time(6767);
	private final BigDecimal price = BigDecimal.valueOf(12.12);
	
	private Offer offer = new Offer();
	
	@Test
	public void testSetGetId() {
		offer.setId(id);
		assertEquals(offer.getId(), id);
	}
	
	@Test
	public void testSetGetFilmId() {
		offer.setFilmId(filmId);
		assertEquals(offer.getFilmId(), filmId);
	}
	
	@Test
	public void testSetGetHallId() {
		offer.setHallId(hallId);
		assertEquals(offer.getHallId(), hallId);
	}
	
	@Test
	public void testSetGetDate() {
		offer.setDate(date);
		assertEquals(offer.getDate(), date);
	}
	
	@Test
	public void testSetGetTime() {
		offer.setTime(time);
		assertEquals(offer.getTime(), time);
	}
	
	@Test
	public void testSetGetPrice() {
		offer.setPrice(price);
		assertEquals(offer.getPrice(), price);
	}
	
	
}
