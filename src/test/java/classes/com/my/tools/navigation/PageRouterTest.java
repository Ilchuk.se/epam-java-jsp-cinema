package classes.com.my.tools.navigation;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

public class PageRouterTest {
	@Test
	public void testCreateLinkMethod() {
		final HttpServletRequest request = mock(HttpServletRequest.class);
		final String URL = "/some/address";
		final String ROOT = "/projectRoot";
		final String expecteAddress = ROOT + URL;
		
		when(request.getContextPath()).thenReturn(ROOT);
		
		String resultURL = PageRouter.createLink(request, URL);
		assertEquals(resultURL, expecteAddress);
		
		verify(request, times(1)).getContextPath();	
	}
	
	@Test
	public void testGetURLMethod() {
		final PageRouter pageRouter = PageRouter.MAIN;
		final String mainPageURL = "/";
		assertEquals(pageRouter.getURL(), mainPageURL);
	}
	
	@Test
	public void testGetPathMethod() {
		final PageRouter pageRouter = PageRouter.MAIN;
		final String mainPagePath = "/jsp/pages/main/MainPage.jsp";
		assertEquals(pageRouter.getPath(), mainPagePath);
	}
}
