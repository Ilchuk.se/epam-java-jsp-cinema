package classes.com.my.controllers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ControllerExceptionTest {
	@Test
	public void testConstructor() {
		final Exception ex = new Exception("Some reason");
		final String message = "some message";
		final ControllerException exception = new ControllerException(message, ex);
		
		assertEquals(exception.getMessage(), message);
		assertEquals(exception.getCause(), ex);
	}
}
