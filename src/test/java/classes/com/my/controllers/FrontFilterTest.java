package classes.com.my.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

public class FrontFilterTest {

	@Test
	public void testInit() throws ServletException {
		FrontFilter filter = new FrontFilter();
		FilterConfig config = mock(FilterConfig.class);
		filter.init(config);
	}
	
	@Test
	public void testDestroy() throws ServletException {
		FrontFilter filter = new FrontFilter();
		filter.destroy();
	}
	
	private HttpServletRequest request = mock(HttpServletRequest.class);
	private HttpServletResponse response = mock(HttpServletResponse.class);
	private FilterChain filterChain = mock(FilterChain.class);
	
	@Test
	public void testDoFilterGetMethod() throws IOException, ServletException {
		StringBuffer URLBuffer = new StringBuffer();
		URLBuffer.append("http://localhost:8080/maven-web/");
		
		when(request.getServletPath()).thenReturn("/");
		when(request.getRequestURL()).thenReturn(URLBuffer);
		when(request.getMethod()).thenReturn("get");
		
		FrontFilter filter = new FrontFilter();
		filter.doFilter(request, response, filterChain);
	}
	
	@Test
	public void testDoFilterGetPost() throws IOException, ServletException {
		StringBuffer URLBuffer = new StringBuffer();
		URLBuffer.append("http://localhost:8080/maven-web/");
		
		when(request.getServletPath()).thenReturn("/");
		when(request.getRequestURL()).thenReturn(URLBuffer);
		when(request.getMethod()).thenReturn("post");
		
		FrontFilter filter = new FrontFilter();
		filter.doFilter(request, response, filterChain);
	}
	
	@Test
	public void testDoFilterUnexistedURL() throws IOException, ServletException {
		StringBuffer URLBuffer = new StringBuffer();
		URLBuffer.append("http://localhost:8080/maven-web/");
		
		when(request.getServletPath()).thenReturn("some unexisted URL");
		when(request.getRequestURL()).thenReturn(URLBuffer);
		when(request.getMethod()).thenReturn("post");
		
		FrontFilter filter = new FrontFilter();
		filter.doFilter(request, response, filterChain);
	}
}
